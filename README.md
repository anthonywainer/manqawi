﻿## mankawi
mankawi Es una plataforma web en la nube, que automatiza los procesos de comercialización de bienes/servicios de cualquier rubro.

### Version
V1

### Tegnologías

* [Django1.10] - Django hace que sea más fácil de construir mejores aplicaciones web más rápido y con menos código.

### Installation
```sh
$ git clone
$ pip install -r requirements.txt
$ --settings=config.settings
```

### Configurar Base de Datos `mankawi/config/db_example.py`

copiar db_example.py a local.py
```sh
$  cp mankawi/config/db_example.py mankawi/config/db.py
DATABASES = {
    'default': {
       'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
       'NAME'    : 'mankawi',
       'USER'    : 'mankawi',
       'PASSWORD': 'mankawi',
       'HOST'    : 'localhost',
       'PORT'    : '5432',
    }
}
```

### Migraciones [warning]

```sh
$  python manage.py makemigrations
$  python manage.py migrate
```

### Crear Super Usuario
```sh
$  python manage.py createsuperuser
```

### agregar data inicial
```sh
$  python start.py
```

### Iniciar
```sh
$ python manage.py runserver www.mankawi.com
```



### Desarrollo
```sh
$ celery worker -A config.celery -l info
```






