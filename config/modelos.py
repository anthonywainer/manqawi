#.-
from apps.admn_caja.models import tipo_movimiento
from apps.admn_caja.models import tipo_pago
from apps.admn_caja.models import comprobante
from apps.admn_caja.models import cajas
from apps.admn_caja.models import conceptos
from apps.admn_caja.models import sesion_caja
from apps.admn_caja.models import movimiento
from apps.servicio.models import tipo_plato
from apps.servicio.models import platos
from apps.servicio.models import combos
from apps.servicio.models import tipo_producto
from apps.servicio.models import productos
from apps.servicio.models import tipo_mesa
from apps.servicio.models import mesas
from apps.servicio.models import mesas
from apps.servicio.models import pedidos
from apps.servicio.models import pedidos
#-

def get_li(name):
    ex = ['id', 'status', 'created_at', 'updated_at', 'deleted_at']
    li = []
    name = eval(name)

    for c in name._meta._get_fields():
        li.append(c.name)
    for d in ex:
        li.remove(d)
    return li