DATABASES = {
    'default': {
       'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
       'NAME'    : 'manqawi',
       'USER'    : 'postgres',
       'PASSWORD': 'postgres',
       'HOST'    : 'localhost',
       'PORT'    : '5432',
    }
}


