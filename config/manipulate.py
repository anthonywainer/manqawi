import os, subprocess, sys
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

if sys.platform == "win32":
    bs = "\\"
else:
    bs = "/"

def create_file(u, s):
    with open(u, "w") as f:
        f.write(s)
        f.close()

def create_forms(app,name):
    forms = "from django import forms\n" \
            "from ..models import "+name+"\n\n" \
            "class "+name+"Form(forms.ModelForm):\n" \
            "  class Meta():\n"\
                        "    model = "+name+"\n" \
                        "    fields = '__all__'\n" \
                        "    exclude = ('status', 'deleted_at')"

    create_file(BASE_DIR+bs+"apps"+bs+app+bs+"forms"+bs+"f_"+name+".py", forms)

def create_tform(name,app,li):
    html_f="<h4 align='center'>{{ name|upper }} "+(name+'s').upper()+" </h4>\n" \
            "<a onclick='atras()' style='color: red; cursor: pointer'>\n" \
                "  <i class='fa fa-backward'></i>\n" \
                "  Atras\n"\
            "</a><br>\n"\
            "<div class='row'>\n"\
                "  <form onsubmit=\"guardarMantenimiento(this,{% if request.GET.id %}{{request.GET.id }}{% else %}''{% endif %},"\
                        "'"+name+"','"+app+"' );return false;\"\n"\
                "  >\n"\
                    "    {% csrf_token %}\n"\
                    "    <div class='col s4'></div>\n"\
                    "    <div class='col s4'>\n"
    for b in li:
        html_f += "      <div class='input-field'>\n" \
                        "        {{ form."+b+" }}\n" \
                        "        {{ form."+b+".label_tag }}\n" \
                    "      </div>\n"

    html_f += "    </div>\n"\
                    "    <div class='col s12' align='center'>\n"\
                        "      <button type='submit' class='waves-effect waves-light btn' id='btn_envio'>\n"\
                            "        <i class='material-icons'>send</i>{{ name }}\n"\
                        "      </button>\n"\
                    "    </div>\n"\
                "  </form>\n"\
            "</div>\n" \
            "<!-- Modal -->\n" \
            "<div id='myModal'> </div>\n" \
            "<script>$('select').select2({width: '100%'});</script>"

    os.mkdir(BASE_DIR + bs+"apps"+bs+app+bs+"templates"+bs+name)
    create_file(BASE_DIR + bs+"apps"+bs + app + bs+"templates"+bs+name+bs+"frm_"+name+".html", str(html_f))

def create_tlist(app,name,li):
    ht = "<a onclick='atras()' style='color: red; cursor: pointer'>\n"\
            "  <i class='fa fa-backward'></i>\n"\
            "  Atras\n"\
        "</a><br>\n" \
        "<h4 align='center' style='color: #3293a2'><b>LISTA DE "+(name+'s').upper()+"</b></h4>\n" \
        "<hr>\n"\
        "<a type='button' class='btn btn-primary' href='#"+app+"/"+""+name+"/"+"add"+"?id='>Agregar</a><br>\n"\
        "<div class='card'>\n"\
            "  <div class='card-content'>\n" \
                "    <table>\n" \
                    "      <thead>\n" \
                        "        <tr>\n" \
                            "          <td>#</td>\n"

    for i in li:
        ht +=  "          <td>"+i+"</td>\n"\

    ht+=                     "          <td>Editar</td>\n" \
                             "          <td>Eliminar</td>\n" \
                        "        </tr>\n" \
                    "      </thead>\n" \
                "    <tbody>\n" \
                        "      {% for foo in object_list %}\n" \
                            "        <tr>\n"\
                                "          <td>{{ forloop.counter }}</td>\n"
    for j in li:
        ht +=  "          <td>{{ foo."+j+"}}</td>\n"\

    ht+=                       "          <td>\n"\
                                    "            <a style='color: red' href='#"+app+"/"+""+name+"/"+"update/{{ foo.id}}/?id={{ foo.id}}'>\n" \
                                        "              <i class='fa fa-pencil-square-o'></i>\n"\
                                    "          </a>\n"\
                                "          </td>\n" \
                                "          <td>\n"\
                                    "            <a style='color: red'  onclick=\"deleteMantenimiento('"+app+"','"+name+"',{{ foo.id}})\" style='cursor: pointer'>\n"\
                                        "              <i class='fa fa-trash puntero'></i>\n"\
                                    "            </a>\n"\
                                "          </td>\n"\
                            "        </tr>\n"\
                        "      {% endfor %}\n"\
                    "    </tbody>\n"\
                "  </table>\n"\
            "</div>\n"\
        "</div>\n"\
        "{% load static %}\n"\
        "<script src='{% static 'assets/datatables/js/datable-es.js' %}'></script>\n"\
        "<script src='{% static 'js/appSecurity/baseMantenimiento.js' %}'></script>"

    if not os.path.exists(BASE_DIR + bs+"apps"+bs + app + bs+"templates"+bs + name):
        os.mkdir(BASE_DIR + bs+"apps"+bs + app + bs+"templates"+bs + name)
    create_file(BASE_DIR + bs+"apps"+bs + app + bs+"templates"+bs + name + bs+"list_" + name + ".html", str(ht))

def create_views(app,name):

    views = "from django.contrib.auth.decorators import login_required\n" \
            "from django.shortcuts import HttpResponse\n" \
            "from apps.core.crud import ListBase, SaveBase, UpdateBase\n" \
            "from ..models import "+name+"\n" \
            "from ..forms.f_"+name+" import "+name+"Form\n\n" \
            "class list_"+name+"(ListBase):\n" \
                "  template_name = '"+name+"/"+"list_"+name+".html'\n" \
                "  queryset = "+name+".objects.filter(status=True).values()\n\n" \
            "class add_"+name+"(SaveBase):\n" \
                "  form_class = "+name+"Form\n" \
                "  template_name = '"+name+"/"+"frm_"+name+".html'\n\n" \
                "class update_"+name+"(UpdateBase):\n" \
                "  form_class = "+name+"Form\n" \
                "  template_name = '"+name+"/"+"frm_"+name+".html'\n" \
                "  model = "+name+"\n\n" \
            "@login_required(login_url='login/')\n" \
            "def delete"+name+"(em, id):\n" \
                "  dp = "+name+".objects.get(pk=id)\n" \
                "  dp.status = False\n" \
                "  dp.save()\n" \
                "  return HttpResponse('DATOS ELIMINADOS') "

    create_file(BASE_DIR+bs+"apps"+bs+app+bs+"views"+bs+"v_"+name+".py", views)


def find_between_r( s, first, last ):
    try:
        start = s.rindex( first )
        end = s.index( last, start )
        print(start,end)
        return s[start:end]
    except ValueError:
        return ""

def read_write(url, i, fn, nt):
    
    f = open(url, encoding='utf-8')
    ss = f.read()

    txtt = find_between_r(ss, i, fn)

    #print(txtt)
    nn = txtt + nt
    with open(url, "r+") as f:
        s = f.read()
        lines = s.replace(txtt, nn)
        f.seek(0)
        f.flush()
        f.write(lines)
        f.close()




def create_app(app):
    #primero creamo la aplicacion
    PATH= "django-admin startapp " #path para ejecutar la creación del path
    #app = "mantenimiento" # nombre de la app

    subprocess.call("cd "+BASE_DIR+bs+"apps && "+PATH+" "+app, shell=True)# ejecucion


    #instalar la app en base.py
    uf = BASE_DIR + bs+"config"+bs
    nt = "'apps."+app+"'"


    read_write(uf+"base.py", "TENANT_APPS = (", ")", "  " + nt + ",\n")
    #instalar la appa en urls.py
    nt = "url(r'^', include('apps."+app+".urls'))"
    read_write(uf+"urls.py", "urlpatterns = [", "]", "  " + nt + ",\n")

    #crear las rutas de las vistas
    st = "from django.conf.urls import url \n#views.\n#.\n\nurlpatterns = [  ]"
    create_file(BASE_DIR+bs+"apps"+bs+app+bs+"urls.py", st)

    #crear y reorganizar
    os.mkdir(BASE_DIR + bs+"apps"+bs+ app + bs+"templates")
    os.mkdir(BASE_DIR + bs+"apps"+bs + app + bs+"views")
    os.mkdir(BASE_DIR + bs+"apps"+bs + app + bs+"forms")
    os.remove(BASE_DIR + bs+"apps"+bs + app + bs+"views.py") #eliminar views.py
    #rmdir, para renombrar archivo
    #if not os.path.exists("templates"):

def create_crud(app,name,li):
    #creamos el archivo forms
    create_forms(app, name)
    #creamos el archivo views y templates
    create_views(app, name)
    create_tform(name, app,li)
    create_tlist(app, name,li)
    uf = BASE_DIR+ bs+"apps"+bs + app
    read_write(uf+bs+"urls.py", "#views.", "#.", 
               "from .views.v_"+name+" import list_"+name+",add_"+name+",update_"+name+",delete"+name+"\n")
    #creamos y agregamos a la urls
    
    ul =    "\n\n  # "+name+"\n"\
            "  url(r'^"+app+"/"+name+"/"+"listar/$', list_"+name+".as_view()),\n"\
            "  url(r'^"+app+"/"+name+"/"+"add/$', add_"+name+".as_view()),\n"\
            "  url(r'^"+app+"/"+name+"/"+"update/(?P<pk>\d+)/$', update_"+name+".as_view()),\n"\
            "  url(r'^"+app+"/"+name+"/"+"delete/(?P<id>\d+)/$', delete"+name+"),\n"


    read_write(uf + bs+"urls.py", "urlpatterns = [", "]",ul)

