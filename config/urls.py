from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.security.urls')),
    url(r'^', include('apps.admn_caja.urls')),
  url(r'^', include('apps.reportes.urls')),
  url(r'^', include('apps.servicio.urls')),
  url(r'^', include('apps.compra.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
