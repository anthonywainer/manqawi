import json

from django.db import transaction, IntegrityError
from apps.security.models import icons

def icon():
    try:
        with transaction.atomic():
            if len(icons.objects.all()) == 0:
                with open('json/icons.json') as f:
                    col = json.load(f)

                for i in col:
                    print(i)
                    c = icons(**i)
                    c.save()
                    print("se agrego el icono " + c.description)

            else:
                raise ValueError("Hay iconos ingresados, limpia la base de datos")

    except IntegrityError as e:
        error = str(e)
        print(error)

