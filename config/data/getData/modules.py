import json

from django.db import transaction, IntegrityError
from apps.security.models import modules

def mod():
    try:
        with transaction.atomic():
            if len(modules.objects.all()) == 0:
                with open('json/modules.json') as f:
                    col = json.load(f)

                for i in col:
                    c = modules(**i)
                    c.save()
                    print("se agrego el módulo " + c.description)

            else:
                raise ValueError("Hay módulos ingresados, limpia la base de datos")

    except IntegrityError as e:
        error = str(e)
        print(error)