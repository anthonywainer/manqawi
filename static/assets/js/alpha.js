$( document ).ready(function() {


    if ($('.material-design-hamburger__icon').length === 1) {
        document.querySelector('.material-design-hamburger__icon').addEventListener(
            'click',
            function() {
                var child;
                document.body.classList.toggle('background--blur');
                this.parentNode.nextElementSibling.classList.toggle('menu--on');

                child = this.childNodes[1].classList;

                if (child.contains('material-design-hamburger__icon--to-arrow')) {
                    child.remove('material-design-hamburger__icon--to-arrow');
                    child.add('material-design-hamburger__icon--from-arrow');
                } else {
                    child.remove('material-design-hamburger__icon--from-arrow');
                    child.add('material-design-hamburger__icon--to-arrow');
                }
            }
        );
    }

    $(".fixed-sidebar .navigation-toggle a").removeClass('button-collapse');
    $(".fixed-sidebar .navigation-toggle a").addClass('reverse-icon');

            $(".fixed-sidebar .navigation-toggle a").click(function() {
                $('#slide-out').toggle();
                $('.mn-inner').toggleClass('hidden-fixed-sidebar');
                $('.mn-content').toggleClass('fixed-sidebar-on-hidden');
                $(document).trigger('fixedSidebarClick');
            });

    if(($(window).width() < 993)&&(!$('.mn-content').hasClass('fixed-sidebar-on-hidden'))){
        $(".fixed-sidebar .navigation-toggle a").click();
        $('.fixed-sidebar .navigation-toggle a span').addClass('material-design-hamburger__icon--to-arrow');
    };

    // Menu
    $('.sidebar-menu > li > a.collapsible-header').click(function() {
        $('.sidebar-menu > li > a.active:not(.collapsible-header)').parent().removeClass('active');
        $('.sidebar-menu > li > a.active:not(.collapsible-header)').removeClass('active');
    });


    // Right Dropdown
    $('.dropdown-right').dropdown({
        alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });

    // Initialize collapse button
    $('.button-collapse:not(.right-sidebar-button)').sideNav();
    $('.button-collapse.right-sidebar-button').sideNav({
        edge: 'right'
    });


    $('.collapsible').collapsible();
      $('.slider').slider({full_width: true});



    $('.left-sidebar-hover').mouseover(function() {
        $('.button-collapse').click();
        $('.material-design-hamburger__layer').removeClass('material-design-hamburger__icon--from-arrow');
        $('.material-design-hamburger__layer').addClass('material-design-hamburger__icon--to-arrow');
        $('#slide-out').addClass('openOnHover');
    $('#slide-out').mouseleave(function() {
        if($(this).hasClass('openOnHover')){
        $('.button-collapse').click();
        $('.material-design-hamburger__layer').addClass('material-design-hamburger__icon--from-arrow');
        $('.material-design-hamburger__layer').removeClass('material-design-hamburger__icon--to-arrow');
        $('#slide-out').removeClass('openOnHover');}
    });

    });

});