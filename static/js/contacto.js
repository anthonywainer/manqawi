function reenvioEmail(t) {

     names=$(t).serialize();
     $.post('/contacto/index/', names).done(function (data) {
         document.getElementById("myFor").reset();
         Materialize.toast(data, 4000);
         return false;
     })
 }
function registrarContacto(t){

    name=$(t).serialize();
    $.post('/contacto/',name).done(function (data) {
        document.getElementById("myForm").reset();
        Materialize.toast(data, 4000);
        return false;
    }).fail(function(resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        console.log(resp);
        for (e in errors) {
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
}

function unblock_form() {
        $('.errorlist').remove();
    }
