function loginP(t) {
    w=$(t).serialize();
    $.post("/login/",w,function(data){
        unblock_form();
        location.reload();
    }).fail(function(resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        console.log(resp);
        for (e in errors) {
            if (e == '__all__'){
                $('#msmlogin').removeClass('hide');
                $('#msmlogin').addClass('alert');
                $('#msmlogin').prepend(errors[e]);
            }
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
    return false;
}
function  contacto(t){
    s = $(t).serialize();
    $.post("contacto/",s).done(function(d){
        unblock_form();
        //imprimir mensaje
    }).fail(function(resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        console.log(resp);
        for (e in errors) {
            if (e == '__all__'){
                $('#msmlogin').html(errors[e]);
            }
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
    return false;

}

function unblock_form() {
        $('.errorlist').remove();
    }



