$("select").material_select();

function SeeSubMod(p,d) {
    $.getJSON('security/modules/jsonsubmod?id='+p, function(data) {
        a = '<a class="waves-effect waves-light btn orange"  onclick = "addSubModForm('+p+',\''+d+'\')"><i class="material-icons left">add</i>SUBMODULO</a>';
       if (data != ''){
           t = _.template(a+$("#tsubmodule").html());
           $("#sub").empty().html(t({data:data,d:d,p:p}));
       }else{
        $("#sub").empty().html(a);
       }
    });
}
function addSubModForm(id,de){
    m =  Modal('idM',"AGREGAR SUB MÓDULO",'',"<input onclick = 'addSubModSave("+id+",\""+de+"\")' type='button' value ='registrar' class='btn btn-primary'>","");
    $('#myModal').html(m);
    $('#idM').openModal();
    t = _.template($("#tFormSubM").html());
    $(".modal-body").empty().html(t({data:null,id:id,de:de}));
    se(".moduleA");
    i =1;
    $('span[dir="ltr"]').each(function() {
        i++;
        if (i == 4){
          $(this).remove();
        }

    });
}
function crear_crud(app,name) {
    $.get('security/modules/crear_crud',{'name':name,'app':app}).done(function (d) {
        noti("success",d);
    });
}

function addSubModSave(p,d){
    f = $(".modal-body #formSubM").serialize()+'&url='+$("#url").val();
    $.post('security/modules/mostrarsudmodulo/',f, function() {
        $.getJSON('security/modules/jsonsubmod?id='+p, function(data) {
           SeeSubMod(p,d)
        });
    }).fail(function(resp) {
        errornormal(resp,"formSubM");
    }).done(function(){
        cerrarModal('idM');
    });
}
//------------editar y eliminar modules hijos------------
function updateSubM(id,de,p){
    $.getJSON("security/modules/editmh","idmh="+id,function(data){
        $.each(data, function(key,value){
           m =  Modal('idM',"ACTUALIZAR SUB MÓDULO",'',"<input onclick = 'updateSaveSubMod("+id+",\""+de+"\","+p+")' type='button' value ='registrar' class='btn btn-primary'>","");
           $('#myModal').html(m);
           $('#idM').openModal();

            //captura evento de selecion en select2
           t = _.template($("#tFormSubM").html());
           $(".modal-body").empty().html(t({data:data,id:p,de:de}));

           se(".moduleA");
           $('#formSubM .select2-selection .select2-selection__rendered').append(value.icon.clase);
           $(".moduleA").on("select2:select",function(e){ $('#formSubM .select2-selection .select2-selection__rendered').empty().append( e.params.data.clase); });
           i =1;
            $('span[dir="ltr"]').each(function() {
                i++;
                if (i == 4){
                  $(this).remove();
                }
            });
        });
    });

}

function updateSaveSubMod(id,d,p){
  f = $("#formSubM").serialize();
  $.post('security/modules/edit/'+id+'/', f+'&mod', function() {
        SeeSubMod(p,d);
  }).fail(function(resp) {
        errornormal(resp,"formSubM");
  }).done(function(a){
        cerrarModal('idM');
  });
}

function deleteSubMod(id,d,p){
    $.get('security/modules/delete/'+id+'/', function(data) {
        SeeSubMod(p,d);
    });
}

function kdes(t,u){
    $(".modal-body #url").val(u+"/"+$(t).val());
}