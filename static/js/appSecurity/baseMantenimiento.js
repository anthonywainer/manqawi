    function Modal(id,title,body,footer,taman){
      var mod = ''+
      '<div class="modal" id="'+id+'"  style="width:'+taman+'; max-height: '+taman+'">' +
        '<div class="modal-dialog">' +
          '<div class="modal-content">' +
            '<div class="modal-header">' +
              '<h4 class="modal-title" align="center">'+title+'</h4>' +
            '</div>' +
            '<div class="modal-body">' + body +'</div>'+
            '<div class="modal-footer">'+footer+'</div>' +
          '</div>' +
        '</div>' +
      '</div>';
      return mod;
    }

    function cerrarModal(id){
      $('#'+id).closeModal();
    }

    function addF(t,a,n,ids,tm,idm) {
        m =  Modal(idm,"AGREGAR "+n.toUpperCase(),'<div class="row"></div>',"",tm);
        $('#myModal').html(m);
        $("#"+idm).openModal();
        $("#myModal .modal-body .row").load(a+"/"+n+"/add/?id= .row", function (d) {
            dd = $(d).find("form").attr("onsubmit","guardarF(this,'"+n+"','"+a+"','"+ids+"','#"+idm+"'); return false");
            $("#myModal .modal-body .row").html(dd);
            $("#myModal select").select2({'width':'100%'});
        });
    }
    function guardarF(tf,n,a,ids,idm){
        d = $(tf).serialize();
        u = a+'/'+n+'/add/';
    l = Lobibox.confirm({
        msg : "¿Está seguro de guardar: ?",
        callback: function ($this, type, ev) {
            if(type=="yes"){
                $.post(u,d).done(function (da) {
                    noti('success', "ok");
                    da = JSON.parse(da);
                    $(ids).select2({width:"100%",
                      data: da
                    }).val(da[0].id).trigger("change");
                    $(idm).closeModal();
                    return false;
                },'json').fail(function(r) {
                        $('.errorlist').remove();
                        var errors = JSON.parse(r.responseText);
                                for (e in errors) {
                                    var id = '#id_' + e;
                                    $(id).after(errors[e]);
                                }
                });
                return false;
            }
        }
    });

}

function todos_se(n) {
        a=[];
        $(n+" option").each(function(){
        a.push($(this).val())
        });
        ninguno_se(n);
        $(n).val(a).trigger("change")
}

function ninguno_se(n){

    $(n).val(null).trigger("change")
    return true;
}
function tt_se(n) {
    ids = capId(n,"select");
    for (var i=0; i<ids.length; i++){
        todos_se(ids[i]);
    };
}
function nn_se(n){
    ids = capId(n,"select");
    for (i=0; i<ids.length; i++){
        ninguno_se(ids[i]);
    }
    return true;
}
function ttt_se(n) {
    idt = capId(n,"table");
    for (var j=0; j<idt.length; j++){
       ids =  capId(idt[j],"select");
        for(var t=0;t<ids.length; t++){
            todos_se(ids[t]);
        }
    }
}
function nnn_se(n) {
    idt = capId(n,"table");
    for (var j=0; j<idt.length; j++){
       ids =  capId(idt[j],"select");
        for(var t=0;t<ids.length; t++){
            ninguno_se(ids[t]);
        }
    }
}
function capId(n,t) {
   a=[];
   $(n+" "+t).each(function(index){
      a.push("#"+$(this).attr("id"));
   });
    return a;
}

function change_url(n,id) {
        url = window.location.hash;
        url=url.split("?");
        url2 = url[0].substr(1, url[0].length);

        if (url.length>1){
            pa = url[1].split("&");
            if (pa.length<=1){
                no = pa[0].split("=");
                if(no[0] == n){
                    nuu = "";
                }else{
                    nuu = pa+"&"
                }
                nu = nuu+n+"="+id;
            }else{
                nu = "";

                bf = nb =false;
                for (i in pa){

                    if (pa[i].split("=")[0] == n){
                        if (i==pa.length-1){
                            bb=""; aa="&"
                        }else {
                            bb="&";
                            aa="";
                            if (i>0) {
                                aa = "&"
                            }
                        }
                        nu+=aa+n+"="+id+bb;
                        bf = true;
                        nb = bf;
                    }else{
                        if (nb){
                            cs ="";
                            nb=false;
                        }else if(i==0){
                            cs=""
                        }else{
                            cs = "&";
                        }
                        nu+=cs+pa[i];
                    }
                }
                if (bf==false){
                    nu+="&"+n+"="+id
                }
            }
        }else{
            nu = n+"="+id;
        }
        window.location.hash=url[0]+"?"+nu;
    }

function deleteMantenimiento(a,n,i){
    l = Lobibox.confirm({
        msg : "¿Está seguro de eliminar: ?",
        callback: function ($this, type, ev) {
            if(type=="yes"){
                $.get(a+'/'+n+'/delete/'+i+'/',function(data){
                    traemedatos(a+"/"+n+"/listar");
                    noti('error',data);
                });
            }
        }
    });
}

$('select').select2({width:'100px'});

function guardarMantenimiento(t,id,n,a){
    $("#btn_envio").attr("disabled","true");
    formData = new FormData($(t)[0]);
    if (id){
        url = a+'/'+n+'/update/'+id+'/';
    }else{
        url = a+'/'+n+'/add/';
    }
    $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,

            success: function(da){
                noti('success', da);
                atras();
                return false;
            },
            error: function (r) {
                $('.errorlist').remove();
                var errors = JSON.parse(r.responseText);
                        for (e in errors) {
                            var id = '#id_' + e;
                            $(id).after(errors[e]);
                        }
                return false;
            }

    });

}

    ar = [];
if (!check_js("/static/js/highcharts/highcharts.js")){
       ar.push( "/static/js/highcharts/highcharts.js" )
    }
    if (!check_js("/static/js/highcharts/exporting.js")){
        ar.push("/static/js/highcharts/exporting.js")
    }
    if (!check_js("/static/js/highcharts/export-csv.js")){
        ar.push("/static/js/highcharts/export-csv.js")
    }
    if (!check_js("/static/js/highcharts/highcharts-3d.js")){
       ar.push("/static/js/highcharts/highcharts-3d.js")
    }
    if (!check_js("/static/js/highcharts/drilldown.js")){
       ar.push("/static/js/highcharts/drilldown.js")
    }


    $.getScript( ar[0] );
    $.getScript( ar[1] );
    $.getScript( ar[2] );
    $.getScript( ar[3] );
    $.getScript( ar[4] );