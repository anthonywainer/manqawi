// --------------------------------------------------------
//	Navigation Bar
// -------------------------------------------------------- 	
$(window).scroll(function(){	
	"use strict";	
	var scroll = $(window).scrollTop();
	if( scroll > 60 ){		
		$(".navbar").addClass("scroll-fixed-navbar");				
	} else {
		$(".navbar").removeClass("scroll-fixed-navbar");
	}
});

// --------------------------------------------------------
//	Smooth Scrolling
// -------------------------------------------------------- 	
/*$("#sidebar-menu li a[href^='#']").on('click', function(e) {
    e.preventDefault();
	$( "#target" ).focus();
    $('#sidebar-menu').fadeIn(1400,"linear");
});*/
