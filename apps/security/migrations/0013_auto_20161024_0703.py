# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-10-24 12:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('security', '0012_auto_20161022_0208'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='module',
        ),
        migrations.RemoveField(
            model_name='users',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='users',
            name='is_superuser',
        ),
        migrations.RemoveField(
            model_name='users',
            name='user_permissions',
        ),
        migrations.AddField(
            model_name='profile',
            name='permisos',
            field=models.ManyToManyField(related_name='detail_profile_permiso', to='security.permiso'),
        ),
    ]
