# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-06 20:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('security', '0020_auto_20161206_1545'),
    ]

    operations = [
        migrations.DeleteModel(
            name='familia',
        ),
        migrations.DeleteModel(
            name='parentesco',
        ),
    ]
