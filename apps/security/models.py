from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin, Group,Permission
)
from django.core import validators
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe
from apps.core.core import TimeStampedModel
#from django_tenants.models import TenantMixin, DomainMixin

'''
class Client(TenantMixin):
    name = models.CharField(max_length=100)
    paid_until =  models.DateField()
    on_trial = models.BooleanField()
    created_on = models.DateField(auto_now_add=True)

    # default true, schema will be automatically created and synced when it is saved
    auto_create_schema = True

class Domain(DomainMixin):
    pass'''

class contacto(TimeStampedModel):
    nombre   = models.CharField(max_length=50)
    email    = models.EmailField()
    telefono = models.IntegerField()
    message  = models.TextField(max_length=200)

class icons(models.Model):
    description = models.CharField(max_length=100)
    clase = models.CharField(max_length=100)
    def __str__(self):
        return mark_safe("<li class='" + self.clase + "'><li> " + self.description)

class modules(TimeStampedModel):
    description = models.CharField(max_length=50)
    father = models.IntegerField(default=0)
    url = models.CharField(max_length=150)
    icon = models.ForeignKey(icons, null=True, related_name='idicon')
    order_mod = models.IntegerField(null=True)
    order_submod = models.IntegerField(null=True)
    def __str__(self):
        return self.description

class permiso(TimeStampedModel):
    descripcion=models.CharField(max_length=50)
    idmodulo=models.ForeignKey(modules)
    def __str__(self):
        return self.descripcion

    def list_permisos(l):
        if l:
            a = ''
            c = 0
            for i in l.keys():
                if c == 0:
                    a += i+"="+str(l[i])
                else:
                    a += ","+i+"="+str(l[i])
                c+=1
            a += ',status = True'
        else:
            a = 'status = True'
        d = eval("permiso.objects.filter("+a+")")
        return d


class profile(TimeStampedModel):
    description = models.CharField(max_length=50)
    permisos      = models.ManyToManyField(permiso,  related_name="detail_profile_permiso")

    def __str__(self):
        return self.description


    def permiso_module(r,self):
        mod = modules.objects.filter(status=True)
        per = permiso.objects.filter(status=True)

        if r.GET.get('id') == "":
            pp = ''
        else:
            pp = profile.objects.filter(pk = r.GET.get('id'), status=True ).values('permisos__descripcion','permisos__idmodulo_id')
            pl = []
            for l in pp:
                pl.append(str(l["permisos__idmodulo_id"])+"-"+l["permisos__descripcion"])

        c = []
        for m in mod:
            if m.father == 0:
                modid = m.id
                b = False
                for o in per:
                    if modid == o.idmodulo_id:
                        if pp:
                            se = str(str(o.idmodulo_id)+"-"+ o.descripcion) in pl
                        else:
                            se = True
                        f = {'id': o.id, 'nombre': o.descripcion, 'selected': se}
                        b = True
                if not b:
                    f = None

                h = []
                for sb in mod:
                    if modid == sb.father:
                        sbid = sb.id
                        pm = []
                        for p in per:
                            if sbid == p.idmodulo_id:
                                if pp:
                                    se = str(str(p.idmodulo_id) + "-" + p.descripcion) in pl
                                else:
                                    se = True
                                pm.append({'id':p.id,'nombre':p.descripcion, 'selected':se})

                        h.append({'id':sb.id,'nombre': sb.description, 'permisos':pm})
                c.append({"padre": m.description,'id':m.id,'idpadre': f, "hijos": h})
        return  c


class MyUserManager(BaseUserManager):
    def create_user(self, email, username, dni, password=None):
        if not email:
            raise ValueError('El usuario tiene una dirección email incorrecta')

        if not dni:
            raise ValueError('El usuario tiene dni incorrecta')

        user = self.model(
            email    = self.normalize_email(email),
            username = username,
            dni      = dni,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, dni, password):
        
        user = self.create_user(email,
            password = password,
            username = username,
            dni      = dni
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class ocupacion(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

sc = (
    ('soltero','soltero'),
    ('casado','casado'),
    ('divorciado','divorciado'),
    ('viudo','viudo'),
)

class nivel_educativo(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

class ubigeo(TimeStampedModel):
    departamento = models.IntegerField(null=True, blank=True)
    provincia    = models.IntegerField(null=True, blank=True)
    distrito     = models.IntegerField(null=True, blank=True)
    nombre       = models.CharField(max_length=100)

    # esto para consulta de departamento general
    def c_departamento(self):
        d = ubigeo.objects.filter(distrito=0, provincia=0).values('departamento','nombre')
        return d

    # consulta de provincia por departamento
    def c_provincia(iddepa):
        p = ubigeo.objects.filter(departamento=iddepa, distrito=0).exclude(provincia=0).values('nombre','provincia')
        return  p

    #cosulta de distrito por provincia y departamento
    def c_distrito(iddepa,idprov):
        di = ubigeo.objects.filter(departamento=iddepa, provincia=idprov).exclude(distrito=0).values('id','nombre')
        return di



class user_blacklist(TimeStampedModel):
    idusuario       =models.ForeignKey('Users')


class empresa(TimeStampedModel):
    titulo = models.CharField(max_length=100)
    about  = models.CharField(max_length=500)
    logo   = models.ImageField(upload_to='logoempresa')

    def __str__(self):
        return self.titulo

    def l_empresa(self):
        return empresa.objects.filter(status=True)[0]


class Users(AbstractBaseUser, TimeStampedModel ):

    ocupacion       = models.ManyToManyField(ocupacion, blank=True)
    estado_civil    = models.CharField(max_length=15,choices=sc, default="Soltero")
    nivel_educativo = models.ForeignKey(nivel_educativo, blank=True, null=True)
    ubigeo          = models.ForeignKey(ubigeo, blank=True, null=True)
    group_blood     = models.CharField(max_length=5, blank=True)
    ruc             = models.CharField(max_length=13, blank=True)


    username = models.CharField(
        ('Usuario'),
        max_length  = 30,
        unique      = True,
        help_text   = ('Requiere: 30 carácteres o menos. Letras, digitos y  @/./+/-/_ sólo.'),
        validators  = [
            validators.RegexValidator(
                r'^[\w.@+-]+$',
               ('Ingrese un usuario válido. Este valor solo podrá contener '
                 'Letras, Números ' 'and @/./+/-/_ Carácteres.')
            ),
        ],
        error_messages={
            'unique': ("Ya existe usuario con ese nombre"),
        },
    )
    names            = models.CharField(max_length=50,null=True, verbose_name='Nombres')
    first_name       = models.CharField(max_length=50,null=True, verbose_name='Apellido Paterno')
    last_name        = models.CharField(max_length=50,null=True, verbose_name='Apellido Materno')
    dni              = models.CharField(
        max_length=8,
        unique=True,
        help_text=('DNI Requiere: 8 carácteres obligatorios'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                ('Ingrese un dni válido. Este valor solo podrá contener '
                 'Números' 'de 8 Dígitos.')
            ),
        ],
        error_messages={
            'unique': ("Ya existe dni, probar con otro"),
        },
    )
    Sex = (
        ('f', 'Femenino'),
        ('m', 'Masculino'),
    )

    email            = models.EmailField(verbose_name='Dirección de email',max_length=255,  blank=True, null=True)
    date_birth       = models.DateField(null=True, verbose_name='Fecha de Cumpleaños')
    foto             = models.ImageField(max_length=500, upload_to='fotoperfil', null=True)
    cellphone        = models.CharField(max_length=100, null=True, verbose_name='Celular')
    telephone        = models.CharField(max_length=100, null=True)
    address          = models.CharField(max_length=200,null=True)
    work_experiencie = models.CharField(max_length=30, null=True)
    sex              = models.CharField(max_length=1, choices=Sex)
    password_default = models.CharField(max_length=15, null=True)

    profiles = models.ManyToManyField(profile, blank=True)
    permisos = models.ManyToManyField(permiso, blank=True)

    is_staff = models.BooleanField(
        ('staff status'),
        default=False,
        help_text=('Indica si el usuario puede iniciar sesión en este sitio de administración.'),
    )
    is_active = models.BooleanField(
        ('active'),
        default=True,
        help_text=(
            'Designa si el usuario debe ser tratado como activo. '
            'Seleccionarla en lugar de eliminar las cuentas .'
        ),
    )
    date_joined = models.DateTimeField(('fecha de Inscripción'), default=timezone.now)

    is_admin         = models.BooleanField(default=False)
    objects          = MyUserManager()

    USERNAME_FIELD   = 'username'
    REQUIRED_FIELDS  = ['dni','email']

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_user_profile(idpro):
        u = Users.objects.filter(status=True, profiles__id=idpro).order_by('-id')
        return u


    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __str__(self):              # __unicode__ on Python 2
        return str(self.dni)+" - "+str(self.first_name.capitalize() + " " +  self.last_name.capitalize() + ", " + self.names.capitalize())

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin






