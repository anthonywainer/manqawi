from django.conf.urls import url
from django.contrib.auth.views import password_change_done
from django.views.generic import TemplateView
from django.contrib.auth.views import login,password_reset,password_reset_done,password_reset_confirm,password_reset_complete
from apps.core.error import error404, error500, error400
from apps.core.pagination.dato_ajax import datos_ajax
from apps.security.views.view_account import (dashboard,editarCuenta, updateCuenta, datoscuenta, fotoperfil, guardaperfil,
                                              register_users, usuariosCompletado, list_users, update_users,
                                              about_users, deleteuser,contactos, list_users_profile,
                                              register_users_type, password_change, addprofile,
                                              deleteuserall,recuperarall,list_usersfalso,contactoGuardar,
                                              permisoPersonalizados,actualizarpermisoPersonalizado
                                              )

from apps.security.views.view_login import (login,LogOut, register)
from apps.security.views.view_module import module, deletemodule,submodule,updatemodule,editamh, fonts, json_submod, v_create_crud
from apps.security.views.view_permission import list_permission, add_permission, update_permission, deletepermission
from apps.security.views.view_profile import (deleteprofile ,SaveBase,UpdateBase,
                                               list_profiles, list_profiles_modules,
                                                TemplateProfile
                                              )
from apps.security.views.view_security import index, listar_prueba, demo,confirmacionemail,hacerrr,restauraContraseña, ok

from apps.security.views.view_mantenimiento import moduloperm,add_nuevos,index_upp,add_user_perfil_perm,update_moduloperm,update_userperm,upd_moduloperm,upd_userperm,index_profilemodul,json_profilemodul,add_profilemodul

urlpatterns = [

    #funciones que no tienen permisos
    url(r'^demo/$', demo.as_view()),
    url(r'^reset/password_reset',password_reset,{'template_name':'passord_reset_form.html','email_template_name':'password_reset_email.html'},name='passord_reset'),
    url(r'^reset/password_res',password_reset_done,{'template_name':'password_reset_done.html'}, name='password_reset_done' ),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm,{'template_name':'password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^reset/done', password_reset_complete,{'template_name':'password_reset_complete.html'}, name='password_reset_complete'),

    url(r'^restauracontraseña/$', restauraContraseña),
    url(r'^restorecontraseña/$', hacerrr),
    url(r'^$', index, name="index" ),
    url(r'^login/$',login.as_view(), name="login"),
    url(r'^dashboard/$',dashboard.as_view(), name="dashboard"),
    url(r'^register/$',register.as_view()),# registro para login del sistema
    #contacto
    #url(r'^contacto/$', FormContacto.as_view()),
    #url(r'^contacto/index/$', indeContacto),
    #url(r'^guardarMensaje/$', guardarMensaje, name='guardarMensaje'),

    #cuenta
    url(r'^security/guardaprofile$', guardaperfil), #actualiza perfil de usuario
    url(r'^security/datoscuenta$', datoscuenta), # datos de la cuenta para ser actualizadps
    url(r'^fotoprofile/$', fotoperfil ), # guardar foto de perfil
    url(r'^password/$', password_change), # cambio de contraseña

    url(r'^password-hecho$', password_change_done, {'template_name': 'system/account/password-hecho.html'},name='password_change_done'),
    url(r'^security/cuenta$', editarCuenta.as_view(), name="editcuenta"),

    url(r'^editarcuenta/$', updateCuenta ),

    url(r'^salir/$', LogOut, name="cerrarsesion"), #cierre de sesión

    #errores
    url(r'^400/$', error400.as_view(), name="400"),
    url(r'^404/$', error404.as_view(), name="404" ),
    url(r'^500/$', error500.as_view(), name="500" ),


    #perfiles
    url(r'^security/profile/vista_profile/$', TemplateProfile.as_view() ),
    url(r'^security/profile/add/$', SaveBase.as_view() ),
    url(r'^security/profile/update/(?P<pk>\d+)/$', UpdateBase.as_view()),
    url(r'^security/profile/list_profile/$', list_profiles),
    url(r'^security/profile/list_profiles_modules/$', list_profiles_modules),
    url(r'^security/profile/delete/(?P<id>\d+)/$', deleteprofile),


    url(r'^securit/permission/ok/$', ok, name='ok'),
    url(r'^security/profile/usuarios_profile/$', list_users_profile ),

    #permission
    url(r'^security/permission/ver_permiso/$', list_permission.as_view()),
    url(r'^matenimientos/permission/listar/$', list_permission.as_view()),
    url(r'^matenimientos/permission/add/$', add_permission.as_view()),
    url(r'^matenimientos/permission/update/(?P<pk>\d+)/$', update_permission.as_view()),
    url(r'^matenimientos/permission/delete/(?P<id>\d+)/$', deletepermission),

    #modules
    url(r'^security/modules/vista_modulo/$', module),
    url(r'^security/modules/delete/(?P<id>\d+)/$', deletemodule),
    url(r'^security/modules/edit/(?P<id>\d+)/$', updatemodule),
    url(r'^security/modules/mostrarsudmodulo/$', submodule ),
    url(r'^security/modules/jsonsubmod$', json_submod ),
    url(r'^security/modules/crear_crud', v_create_crud ),
    url(r'^security/modules/fonts/$', fonts ),
    url(r'^security/modules/editmh/$', editamh ),

    #users
    #url(r'^security/guardarpadres$', guardarpadre ),
    url(r'^security/usuarios/register_users$', register_users.as_view()),
    #url(r'^security/usuarios/addtype/$', register_users_type.as_view()),
    url(r'^security/usuarios/actualizarUsuario/(?P<pk>[0-9]+)/$', update_users.as_view()),
    url(r'^security/usuarios/listar/$', list_users.as_view()),
    url(r'^security/usuarios/about_users', about_users.as_view()),
    url(r'^security/usuarios/cargarfalso$', list_usersfalso),
    url(r'^security/usuarios/eliminarUsuario/(?P<id>\d+)/$', deleteuser),
    url(r'^security/usuarios/deleteall/$', deleteuserall),
    url(r'^security/usuarios/recuperarall/$', recuperarall),
    url(r'^security/usuarios/completado$', usuariosCompletado, name="user-completado" ),
    url(r'^security/usuarios/Buscar', datos_ajax ),

    url(r'^Buscarsecurity/profile/user$',datos_ajax),
    url(r'^seguridad/prueba$',listar_prueba),
    url(r'^security/usuarios/permisoPersoalizados$', permisoPersonalizados),
    url(r'^security/usuarios/actualizarpermisoPersonalizado$', actualizarpermisoPersonalizado),


    #mantenimiento
    url(r'^mantenimiento/moduloperm$', moduloperm),
    url(r'^mantenimiento/moduloperm/add_nuevos/$', add_nuevos),
    url(r'^mantenimiento/moduloperm/update_moduloperm/$', update_moduloperm),
    url(r'^mantenimiento/moduloperm/upd_moduloperm/$', upd_moduloperm),

    url(r'^mantenimiento/userperm$', index_upp),
    url(r'^mantenimiento/userperm/add_userperm/$', add_user_perfil_perm),
    url(r'^mantenimiento/userperm/update_userperm/$', update_userperm),
    url(r'^mantenimiento/userperm/upd_userperm/$', upd_userperm),

    url(r'^mantenimiento/profilemodul$', index_profilemodul),
    url(r'^mantenimiento/profilemodul/json_profilemodul/$', json_profilemodul),
    url(r'^mantenimiento/profilemodul/add_profilemodul/$', add_profilemodul)

]
