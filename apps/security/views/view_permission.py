from django.shortcuts import HttpResponse
from django.views.generic import ListView
from braces.views import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required

from apps.security.models import permiso, modules
from apps.security.forms import permissionForm
from apps.security.views.view_profile import SaveBase, UpdateBase

class list_permission(LoginRequiredMixin, ListView ):
    login_url = reverse_lazy('login')

    template_name = "system/permission/list_permission.html"
    model = permiso
    queryset = permiso.objects.filter(status=True).values('id','descripcion',  'idmodulo__description')

    def get_context_data(self, **kwargs):
        c = super(list_permission, self).get_context_data(**kwargs)
        module = modules.objects.filter(status=True)
        l = {}
        if self.request.GET.get('id_module'):
            l["idmodulo_id"] = self.request.GET.get('id_module')

        queryset = permiso.list_permisos(l)

        c.update({ 'module': module,'object_list': queryset})

        return c


class add_permission(SaveBase):
    form_class = permissionForm
    template_name = "system/permission/frm_permission.html"

class update_permission(UpdateBase):
    form_class = permissionForm
    template_name = "system/permission/frm_permission.html"
    model = permiso

@login_required(login_url='login/')
def deletepermission(em, id):
    dp = permiso.objects.get(pk=id)
    dp.status = False
    dp.save()

    return HttpResponse('DATOS ELIMINADOS')

