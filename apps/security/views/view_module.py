from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, HttpResponse
from django.views.generic import TemplateView
from apps.core.core import Core
from django.http import HttpResponseBadRequest
from rest_framework import permissions
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.renderers import JSONRenderer,TemplateHTMLRenderer
from rest_framework.response import Response
from django.contrib.contenttypes.models import ContentType
from apps.security.forms import UserChangeForm, formFoto, moduleAdd, submoduleAdd
from apps.security.models import icons, Users, modules, permiso
from apps.security.serializers import iconosSerializer, modulesSeria
from config.manipulate import create_app,create_crud, read_write
from config.modelos import get_li

import json
def error(f):
    ee = {}
    for i in f.errors:
        e = f.errors[i]
        ee[i] = str(e)
    return HttpResponseBadRequest(json.dumps(ee))

@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def module(m):
    o = modules.objects.select_related('icon').filter(status = True).values(
        'id','description','url','father',
        'icon__clase'
        )
    f  = moduleAdd()
    if m.method == 'POST':
        f = moduleAdd(m.POST)
        if f.is_valid():
            s = f.save()
            if m.POST.get('app') == "on":
                create_app(s.description)
            p = permiso()
            p.idmodulo_id = s.id
            p.descripcion = "listar"
            p.save()
            return HttpResponse("módulo creado correctamente")
        else:
            return error(f)

    elif m.accepted_renderer.format == 'json':
        o = modules.objects.select_related('icon').filter(status = True)
        serializers = modulesSeria(instance = o, many = True )
        data = serializers.data
        return Response(data)

    return render(m, 'system/module/t_module.html', {'module': o, 'formulario': f })

@login_required(login_url='login/')
def deletemodule(em, id):
    e = modules.objects.get(pk = id)
    e.status = False
    e.save()
    return HttpResponse(e.father)


@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def updatemodule(ed, id):

    if ed.accepted_renderer.format == 'json':
        su = modules.objects.select_related('icon').filter(status = True,id = id)
        serializers = modulesSeria(instance = su, many = True )
        data = serializers.data
        return Response(data)
    if ed.method == 'POST':
        a=get_object_or_404(modules,pk=id)
        if  ed.POST['mod']:
            f = moduleAdd(ed.POST, instance=a)
        else:
            f = submoduleAdd(ed.POST, instance=a)

        if f.is_valid():
           f.save()
        else:
            return error(f)
    return HttpResponse('DATOS EDITADOS')


@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def editamh(r):
    idm=r.GET.get("idmh")

    dato=modules.objects.select_related('icon').filter(id=idm)

    serializers = modulesSeria(instance = dato, many = True )
    hdata = serializers.data
    return Response(hdata)

import os, time
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
@login_required(login_url='login/')
def submodule(sub):
    if sub.method == 'POST':
        f = submoduleAdd(sub.POST)
        if f.is_valid():
            f = f.save()
            if sub.POST.get('crud'):
                app = sub.POST.get("modulo")
                name = f.description
                uf = BASE_DIR + "/../../config/"
                read_write(uf + "modelos.py", "#.-", "#-","from apps."+app+".models import "+name+"\n")
                for i in ["listar",'add','update','delete']:
                    p = permiso()
                    p.idmodulo_id = f.id
                    p.descripcion = i
                    p.save()

            return HttpResponse("ok")
        else:
            return error(f)

@login_required(login_url='login/')
def v_create_crud(r):
    app = r.GET.get("app")
    name = r.GET.get("name")
    li = get_li(name)
    create_crud(app,name,li)
    return HttpResponse("ok")

@login_required(login_url='login/')
def json_submod(r):
    su = modules.objects.select_related('icon').filter(status = True, father = r.GET.get('id')).values('id',
        'description','icon__clase')
    data = json.dumps(list(su))
    return HttpResponse(data)

@login_required(login_url='login/')
@api_view(['GET','POST'])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def fonts(r):
    t  = r.GET.get("q")
    pg = r.GET.get("page")

    if t is not None:
        p = icons.objects.filter( Q(description__contains=t) )[:pg]
    else:
        p = icons.objects.all()[:pg]

    serializers = iconosSerializer(instance = p, many = True )
    data = serializers.data
    return Response(data)

