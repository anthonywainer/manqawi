from django.shortcuts import render, get_object_or_404, HttpResponse
from django.core.mail import send_mail
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.db import connection
from apps.security.forms import UserChangeForm, formFoto
from apps.security.models import Users, modules, empresa #detail_permission_profile_modules,det_user_permiso
from apps.core.pagination.num_pag import paginacion
from django.db.models import Count

import datetime
from itertools import chain

def ok(o):
    return HttpResponse("ok")

def system(r):
    u = r.user.profiles
    template_name = 'system/index.html'
    if len(u.filter(status=True)) > 1:
        idprofile = r.GET.get("sys")

        if idprofile is None:
            p = {'profile':u.filter(status=True)}
            template_name = 'system/system.html'
            #template_name = 'system/systemprueba.html'
            return render(r,template_name,p)

    else:
        idprofile = u.filter(status=True)[0].id

    queryset1 = u.filter(pk = idprofile, status=True, permisos__status=True, permisos__idmodulo__status=True).values(
        'permisos__idmodulo__description', 'permisos__idmodulo__url', 'permisos__idmodulo__father',
        'permisos__idmodulo__icon__clase', 'permisos__idmodulo_id','id', 'description'
    ).annotate(dcount=Count('permisos__idmodulo__description')).order_by('permisos__idmodulo__order_mod', 'permisos__idmodulo__order_submod')

    queryset2 = r.user.permisos.values(
        'idmodulo__description', 'idmodulo__url', 'idmodulo__father',
        'idmodulo__icon__clase', 'idmodulo_id'
    ).filter(idmodulo__status=True)


    # union de las dos consultas identificando los datos de la lista iguales para no agregarlo
    modulos = []
    for datoquery1 in queryset1:
        if datoquery1 not in modulos:
            modulos.append(datoquery1)
    for datoquery in queryset2:
        if datoquery not in modulos:
            modulos.append(datoquery)

    #return render(r, 'system/index.html', {'modulo': modulos})
    profile_des = modulos[0]['description']
    p = {'modulo':modulos, 'profile':u.filter(status=True),'profile_des':profile_des}
    return render(r, template_name, p)


#@login_required(login_url='login/')
from apps.security.forms import LoginForm
def index(r):
    if r.user.is_authenticated():
        return system(r)
    else:
        template_name= "indexdemo.html"
        l = LoginForm
        p={'form':l,'empresa':empresa.l_empresa('')}
        return render(r,template_name,p)

class demo(TemplateView):
    template_name = "indexdemo.html"

def restauraContraseña(r):
    return HttpResponse('hola')

def hacerrr(r):
    for i in r.GET:
       j=i[0]

    return render(r,"newcontraseña.html",{'identificador':j})
def confirmacionemail(r):

    u=Users.objects.filter(email=r.POST.get('email'))
    send_mail(
        'Empresa PeruCore le da la bienvenida!',
        'Gracias por contactarse con nosotros,  http://127.0.0.1:8000/restorecontrase%C3%B1a/?1',
        'perucoresoft@gmail.com',
        [r.POST.get('email')],
        fail_silently=False,
    )

    return HttpResponse ("llego")
def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

def lmodulos(m):
    return render(m, 'system/module/modules.html')


def fotoperfil(r):
    a     = get_object_or_404(Users,pk=r.user.id)
    formu = formFoto(r.POST, r.FILES, instance=a)
    if formu.is_valid():
        formu.save()
    return HttpResponse("ok")

def usuario(r):
    return render(r, "system/account/usuario.html")


def guardarpadre(r):
    f=UserChangeForm(r.POST)
    f.save()
    return HttpResponse("guardado correctamente")

def listar_prueba(r):
    template_name = "system/users/t_modules.html"
    u=modules.objects.filter(status=True).values()
    #u = Users.objects.filter(status=True)
    for fila in u:
        print(fila)
    l= {'search': "'modules','[\"description\",\"url\"]'"}
    v = {'values': "'None'",'template':"'datoAjaxModuless.html'",}
    l.update(v)
    return paginacion(r,u,"",template_name,l)



