from apps.security.models import profile
from django.shortcuts import render

class MiddelwareFunciones(object):
    def __init__(self, get_response=None):
        self.get_response = get_response

    def process_request(self,request):

        if request.user.is_authenticated():

            url=str(request.get_full_path())
            consult = request.user.profiles.all()
            i = 0
            for c in consult:
                if i == 0:
                    q =  c.permisos.all().values('descripcion','idmodulo__url')
                i+=1
                q = q | c.permisos.all().values('descripcion','idmodulo__url')

            consult = q

            url = url.split("?")[0][1:len(url)]
            url = url.split("/")

            urlexepciones = ['reset','admin','editarcuenta','dashboard']
            if ((url[len(url)-1]).find(".")==-1) and not url[0] in urlexepciones:
                if len(url)>=3:
                    for dato in consult:
                        uu = dato["idmodulo__url"].split('/')
                        if str(uu[1])+'-'+str(dato["descripcion"]) == str(url[1])+'-'+str(url[2]):
                            a = True
                            break
                        else:
                            a = False
                    if a:
                        return None
                    else:
                        return render(request,'error/error400.html')
            else:
                return None

