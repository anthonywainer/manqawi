from django.shortcuts import render, get_object_or_404, HttpResponse
from braces.views import LoginRequiredMixin
from django.db.models import Sum
from django.views.generic import TemplateView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from ...admn_caja.models import sesion_caja
import json

class reporte (LoginRequiredMixin,TemplateView):
    login_url = reverse_lazy('login')
    template_name = 'ReporteCaja.html'

@login_required(login_url='login/')
def listasesiones(request):
  lista = sesion_caja.objects.values('fecha','status').annotate(dcount=Sum('montocierre')).order_by("-fecha")

  la = {}
  lt = {}
  mo = {'1':"Enero",'2':'Febrero','3':'Marzo','4':'Abril',
        '5':'Mayo','6':'Junio','7':'Julio','8':'Agosto',
        '9':'Setiembre','10':'Octubre','11':'Noviembre','12':'Diciembre'
        }
  sum = 0
  for li in lista:
    #  print(str(li['fecha'].day)+'-'+str(li['fecha'].month)+'-'+str(li['fecha'].year))
    if mo[str(li['fecha'].month)] in la.keys():
      la[mo[str(li['fecha'].month)]].append([str(li['fecha'].day),li['dcount']])
    else:
        la[mo[str(li['fecha'].month)]] = [[str(li['fecha'].day), li['dcount']]]
 # print(la)
  lta = []
  for key, value in la.items():
      lta.append({
          'name':key,
          'data':value,
          'id':key
      })
 # print(lta)
  s = 0
  for aa in lta:
     for ss in aa['data']:
         s+=ss[1]
     lt[aa['id']] =s
  ltt = []
  for key, value in lt.items():
      ltt.append({
          'name':key,
          'y':value,
          'drilldown':key
      })

  un = [{
      'mes':lta,
      'total':ltt
  }]
  print(un)
  data = json.dumps(un)
  return HttpResponse(data)