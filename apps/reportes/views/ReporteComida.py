from django.shortcuts import render, get_object_or_404, HttpResponse
from braces.views import LoginRequiredMixin
from django.views.generic import TemplateView
import json

class reporteComida (TemplateView):
    template_name = 'ReporteComida.html'