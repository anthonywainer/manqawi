from django.conf.urls import url
from .views.ReporteCaja import reporte, listasesiones
from .views.ReporteComida import reporteComida
urlpatterns = [
    #reporte Caja
    url(r'^reportes/ReporteCaja/mostrar/$', reporte.as_view()),
    url(r'^reportes/ReporteCaja/json/$', listasesiones),
    #reporte Comida
    url(r'^reportes/ReporteComida/show/$', reporteComida.as_view())
]