from django.conf.urls import url 
#views.
from .views.v_compra import list_compra, add_compra, update_compra, delete_compra,infoproducto,guardar_detalle, ver_detalle
from .views.v_proveedor import list_proveedor, add_proveedor, update_proveedor, delete_proveedor

urlpatterns = [
    # compra
    url(r'^compra/compra/listar/$', list_compra.as_view()),
    url(r'^compra/compra/add/$', add_compra.as_view()),
    url(r'^compra/compra/update/(?P<pk>\d+)/$', update_compra.as_view()),
    url(r'^compra/compra/delete/(?P<id>\d+)/$', delete_compra),
    url(r'^compra/compra/infoproducto/$',infoproducto),
    url(r'^compra/compra/guardardetalle/$',guardar_detalle),
    url(r'^compra/compra/verdetalle/$',ver_detalle),

    #proveedor
    url(r'^compra/proveedor/listar/$', list_proveedor.as_view()),
    url(r'^compra/proveedor/add/$', add_proveedor.as_view()),
    url(r'^compra/proveedor/update/(?P<pk>\d+)/$', update_proveedor.as_view()),
    url(r'^compra/proveedor/delete/(?P<id>\d+)/$', delete_proveedor)
]