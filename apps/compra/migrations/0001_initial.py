# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-10 04:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('servicio', '0006_merge_20170109_0026'),
        ('admn_caja', '0005_auto_20170104_1531'),
    ]

    operations = [
        migrations.CreateModel(
            name='compra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('descripcion', models.CharField(max_length=100)),
                ('fechacompra', models.DateField()),
                ('cantidad', models.IntegerField()),
                ('monto', models.DecimalField(decimal_places=2, max_digits=6)),
                ('igv', models.DecimalField(decimal_places=2, max_digits=6)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='cuotascompra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('nrocuota', models.IntegerField()),
                ('montocuota', models.DecimalField(decimal_places=2, max_digits=6)),
                ('montocancelado', models.DecimalField(decimal_places=2, max_digits=6)),
                ('fechavencimiento', models.DateField()),
                ('fechacanselado', models.DateField()),
                ('idcompra', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='compra.compra')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='detalle_amortizacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('monto', models.DecimalField(decimal_places=2, max_digits=6)),
                ('idcuota', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='compra.cuotascompra')),
                ('idmovimiento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admn_caja.movimiento')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='detalle_compra_producto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('cantidad', models.IntegerField()),
                ('precio', models.DecimalField(decimal_places=2, max_digits=6)),
                ('ivg', models.DecimalField(decimal_places=2, max_digits=6)),
                ('idcompra', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='compra.compra')),
                ('idproducto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicio.productos')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
