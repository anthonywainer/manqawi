from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import proveedor
from ...servicio.models import productos
from ..forms.f_proveedor import proveedorForm

class list_proveedor(ListBase):
  template_name = 'proveedor/list_proveedor.html'
  queryset = proveedor.objects.filter(status=True).values()
#  print(queryset.query)

class add_proveedor(SaveBase):
  form_class = proveedorForm
  template_name = 'proveedor/frm_proveedor.html'

class update_proveedor(UpdateBase):
  form_class = proveedorForm
  template_name = 'proveedor/frm_proveedor.html'
  model = proveedor

@login_required(login_url='login/')
def delete_proveedor(em, id):
  dp = proveedor.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS')