from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import compra, proveedor, detalle_compra_producto
from ...servicio.models import productos
from ..forms.f_compra import compraForm
from django.core.serializers.json import DjangoJSONEncoder
from decimal import Decimal
import json

class list_compra(ListBase):
  template_name = 'compras/list_compra.html'
  queryset = compra.objects.filter(status=True)
#  print(queryset.query)

class add_compra(SaveBase):
  form_class = compraForm
  template_name = 'compras/frm_compra.html'

  def get_context_data(self, **kwargs):
       context = super(add_compra, self).get_context_data(**kwargs)
     #  context['producto'] =[{'descripcion': '---------'}]
       context['producto'] = productos.objects.filter(status=True).values('id','descripcion')
    #   print(context['producto'])
       return context

class update_compra(UpdateBase):
  form_class = compraForm
  template_name = 'compras/frm_compra.html'
  model = compra

  def get_context_data(self, **kwargs):
    context = super(update_compra, self).get_context_data(**kwargs)
    context['producto'] = productos.objects.filter(status=True).values('id','descripcion')
    return context

@login_required(login_url='login/')
def delete_compra(em, id):
  #print(em)
  dp = compra.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS')

@login_required(login_url='login/')
def infoproducto(request):
  data = productos.objects.filter(id=request.GET["idpro"]).values('precio')
  return HttpResponse(json.dumps(list(data),cls=DjangoJSONEncoder))

@login_required(login_url='login/')
def guardar_detalle(request):
  n_com = compra()
  n_com.descripcion = request.POST['descripcion']
  n_com.fechacompra = request.POST['fechacompra']
  n_com.totaligv = request.POST['totaligv']
  n_com.totalcompra = request.POST['totalcompra']
  n_com.nombreproveedor_id = request.POST['nombreproveedor']
  n_com.save()
  idc = compra.objects.latest('id')

  print(request.POST)
  re = {}
  for x,y,z,p in zip(request.POST.getlist('idproductitos[]'), request.POST.getlist('cantidades[]'), request.POST.getlist('precios[]'), request.POST.getlist('igvtotal[]')):
    re['idproducto_id'] = int(x)
    re['cantidad'] = int(y)
    re['precio'] = Decimal(z)
    re['ivg'] = Decimal(p)
    re['idcompra_id'] = idc.id
    detalle_compra_producto(**re).save()

  return HttpResponse('compra guardada')

@login_required(login_url='login/')
def ver_detalle(request):
  listacom = detalle_compra_producto.objects.filter(idcompra__fechacompra=request.GET["fecha"],idcompra=request.GET["id"], status=True).values\
    ('idcompra__nombreproveedor__razon_social','idcompra__descripcion','idcompra__fechacompra','idcompra__totalcompra'
     , 'idcompra__totaligv','idproducto__descripcion','cantidad','precio','ivg','status','idcompra')
  data = [
    {
      'detalle': list(listacom)
    }
  ]
  return HttpResponse(json.dumps(data,cls=DjangoJSONEncoder))