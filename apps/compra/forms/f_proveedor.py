from django import forms
from ..models import proveedor

class proveedorForm(forms.ModelForm):
  class Meta():
    model = proveedor
    fields = '__all__'
    exclude = ('status', 'created_at', 'updated_at', 'deleted_at')