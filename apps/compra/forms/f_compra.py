from django import forms
from ..models import compra

class compraForm(forms.ModelForm):
  class Meta():
    model = compra
    fields = '__all__'
    widgets = {
      'fechacompra': forms.DateInput(attrs={'class': 'datepicker'})
    }
    exclude = ('status','created_at', 'updated_at', 'deleted_at')