from django.db import models
from apps.core.core import TimeStampedModel
from apps.servicio.models import productos
from apps.admn_caja.models import movimiento

class proveedor(TimeStampedModel):
    razon_social=models.CharField(max_length=100,blank=True,null=True)
    direcion=models.CharField(max_length=100,blank=True,null=True)
    celular=models.CharField(max_length=10,blank=True,null=True)
    ruc=models.CharField(max_length=11,blank=True,null=True)
    def __str__(self):
        return self.razon_social

class compra(TimeStampedModel):
    nombreproveedor=models.ForeignKey(proveedor,blank=True,null=True)
    descripcion= models.CharField(max_length=100)
    fechacompra=models.DateField()
    totalcompra=models.DecimalField(decimal_places=2, max_digits=6)
    totaligv=models.DecimalField(decimal_places=2, max_digits=6)

    def __str__(self):
        return self.descripcion

    def l_compra(self):
        return compra.objects.filter(status=True)

class cuotascompra(TimeStampedModel):
    nrocuota=models.IntegerField()
    montocuota=models.DecimalField(decimal_places=2, max_digits=6)
    montocancelado=models.DecimalField(decimal_places=2, max_digits=6)
    fechavencimiento=models.DateField()
    fechacanselado=models.DateField()
    idcompra=models.ForeignKey(compra)

class detalle_amortizacion(TimeStampedModel):
    monto=models.DecimalField(decimal_places=2, max_digits=6)
    idmovimiento=models.ForeignKey(movimiento)
    idcuota=models.ForeignKey(cuotascompra)

class detalle_compra_producto(TimeStampedModel):

    cantidad=models.IntegerField()
    precio=models.DecimalField(decimal_places=2, max_digits=6)
    ivg=models.DecimalField(decimal_places=2, max_digits=6)
    idcompra=models.ForeignKey(compra)
    idproducto=models.ForeignKey(productos)
