"""from django.views import View
from braces.views import LoginRequiredMixin
from django.shortcuts import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json


class JsonDetail(View, LoginRequiredMixin):
    queryset = ""
    re       = ""

    def get(self,  *args, **kwargs):
        aj = list(self.queryset)
        return HttpResponse(json.dumps(aj, cls=DjangoJSONEncoder))


"""