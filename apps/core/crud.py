from braces.views import LoginRequiredMixin

from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView
from django.core.paginator import InvalidPage, Paginator, EmptyPage
from django.views.generic.edit import FormView, UpdateView
from django.http import HttpResponseRedirect
from apps.core.error import error


class ListBase(TemplateView, LoginRequiredMixin):
    login_url = reverse_lazy('login')
    template_name = ""
    queryset = None
    paginate_by = 10
    search = ''

    def get_context_data(self, **kwargs):

        if self.request.GET.get('pag'):
            pag = self.request.GET.get('pag')
            page_size = int(pag)
        else:
            page_size = self.paginate_by

        paginator = Paginator(self.queryset, page_size)
        page = 1
        pag = self.request.GET.get('page')
        if pag is not None:
            page = pag
        try:
            lista = paginator.page(page)
        except (EmptyPage, InvalidPage):
            lista = paginator.page(paginator.num_pages)

        r = lista.paginator.num_pages
        context = {
                'page_obj' : lista,
                'pag'    : page_size,
                'range':  range(1, r + 1),
                'object_list': lista
            }
        if self.search:
            context.update(self.search)

        context.update(kwargs)
        return super(ListBase, self).get_context_data(**context)

class SaveBase(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('login')
    form_class = ""
    template_name = ""
    success_url = reverse_lazy('ok')

    def get_context_data(self, **kwargs):
        context = super(SaveBase, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return error(form)


class UpdateBase(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('login')
    template_name = ""
    success_url = reverse_lazy('ok')

    def get_context_data(self, **kwargs):
        context = super(UpdateBase, self).get_context_data(**kwargs)
        context['name'] = 'Actualizar'
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return error(form)

