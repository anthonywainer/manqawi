from django.conf.urls import url 
#views.
from .views.v_tipo_movimiento import list_tipo_movimiento,add_tipo_movimiento,update_tipo_movimiento,deletetipo_movimiento
from .views.v_tipo_pago import list_tipo_pago,add_tipo_pago,update_tipo_pago,deletetipo_pago
from .views.v_comprobante import list_comprobante,add_comprobante,update_comprobante,deletecomprobante
from .views.v_cajas import list_cajas,add_cajas,update_cajas,deletecajas,listasesiones,aperturar,cerrar
from .views.v_conceptos import list_conceptos,add_conceptos,update_conceptos,deleteconceptos
from .views.v_movimientos import *
#.

urlpatterns = [
  # tipo_movimiento
  url(r'^admn_caja/tipo_movimiento/listar/$', list_tipo_movimiento.as_view()),
  url(r'^admn_caja/tipo_movimiento/add/$', add_tipo_movimiento.as_view()),
  url(r'^admn_caja/tipo_movimiento/update/(?P<pk>\d+)/$', update_tipo_movimiento.as_view()),
  url(r'^admn_caja/tipo_movimiento/delete/(?P<id>\d+)/$', deletetipo_movimiento),


  # tipo_pago
  url(r'^admn_caja/tipo_pago/listar/$', list_tipo_pago.as_view()),
  url(r'^admn_caja/tipo_pago/add/$', add_tipo_pago.as_view()),
  url(r'^admn_caja/tipo_pago/update/(?P<pk>\d+)/$', update_tipo_pago.as_view()),
  url(r'^admn_caja/tipo_pago/delete/(?P<id>\d+)/$', deletetipo_pago),


  # comprobante
  url(r'^admn_caja/comprobante/listar/$', list_comprobante.as_view()),
  url(r'^admn_caja/comprobante/add/$', add_comprobante.as_view()),
  url(r'^admn_caja/comprobante/update/(?P<pk>\d+)/$', update_comprobante.as_view()),
  url(r'^admn_caja/comprobante/delete/(?P<id>\d+)/$', deletecomprobante),


  # cajas
  url(r'^admn_caja/cajas/listar/$', list_cajas.as_view()),
  url(r'^admn_caja/cajas/add/$', add_cajas.as_view()),
  url(r'^admn_caja/cajas/update/(?P<pk>\d+)/$', update_cajas.as_view()),
  url(r'^admn_caja/cajas/delete/(?P<id>\d+)/$', deletecajas),

  # sesiones cajas
  url(r'^admn_caja/sesion_caja/listar/$', listasesiones),
  url(r'^admn_caja/sesion_caja/aperturar/$', aperturar),
  url(r'^admn_caja/sesion_caja/cerrar/$', cerrar),

  # conceptos
  url(r'^admn_caja/conceptos/listar/$', list_conceptos.as_view()),
  url(r'^admn_caja/conceptos/add/$', add_conceptos.as_view()),
  url(r'^admn_caja/conceptos/update/(?P<pk>\d+)/$', update_conceptos.as_view()),
  url(r'^admn_caja/conceptos/delete/(?P<id>\d+)/$', deleteconceptos),

  # movimientos
  url(r'^admn_caja/movimientos/listar/$', movimientos_hoy),
  url(r'^admn_caja/movimientos/historial/$', movimientos_historial),
  url(r'^admn_caja/movimientos/validarcaja/$', validarcaja),
  url(r'^admn_caja/movimientos/add/$', add_movimiento),
  url(r'^admn_caja/movimientos/extornarmovi/$', extornarmovi),
  url(r'^admn_caja/movimientos/conceptos_tipo/$', conceptos_tipo),
  url(r'^admn_caja/movimientos/guardarmovimiento/$', guardarmovimiento),
]