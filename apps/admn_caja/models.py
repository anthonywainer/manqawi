from django.db import models
from apps.core.core import TimeStampedModel

# Create your models here.
class tipo_movimiento(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def l_tipo_mo(self):
       return tipo_movimiento.objects.filter(status=True)

class tipo_pago(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def l_tipo_pa(self):
       return tipo_pago.objects.filter(status=True)

class comprobante(TimeStampedModel):
    descripcion = models.CharField(max_length=100)
    correlativo_inicial = models.CharField(max_length=100,default='0000001')
    correlativo = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def l_comprobante(self):
       return comprobante.objects.filter(status=True)

class cajas(TimeStampedModel):
    descripcion = models.CharField(max_length=100)
    montoingresos = models.FloatField(default=0.00)
    montoegresos = models.FloatField(default=0.00)
    montototal = models.FloatField(default=0.00)

    def __str__(self):
        return self.descripcion

    def l_cajas(self):
       return cajas.objects.filter(status=True)

class conceptos(TimeStampedModel):
    descripcion = models.CharField(max_length=100)
    idtipo_movimiento = models.ForeignKey(tipo_movimiento)

    def __str__(self):
        return self.descripcion

    def l_conceptos(self):
       return conceptos.objects.filter(status=True)

class sesion_caja(TimeStampedModel):
    idcaja = models.ForeignKey('cajas')
    idusuario = models.ForeignKey('security.Users')
    fecha = models.DateField()
    montoapertura = models.FloatField()
    montocierre = models.FloatField()

    def __str__(self):
        return self.fecha

class movimiento(TimeStampedModel):
    idconcepto     = models.ForeignKey('conceptos')
    idcomprobante  = models.ForeignKey('comprobante')
    idsesioncaja   = models.ForeignKey('sesion_caja')
    nrocomprobante = models.CharField(max_length=100)
    descripcion  = models.CharField(max_length=100)
    fecha        = models.DateField()
    monto        = models.FloatField()

    def __str__(self):
        return self.monto

    def l_movimiento(self):
       return movimiento.objects.filter(status=True)