from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import tipo_movimiento
from ..forms.f_tipo_movimiento import tipo_movimientoForm

class list_tipo_movimiento(ListBase):
  template_name = 'tipo_movimiento/list_tipo_movimiento.html'
  queryset = tipo_movimiento.objects.filter(status=True).values()

class add_tipo_movimiento(SaveBase):
  form_class = tipo_movimientoForm
  template_name = 'tipo_movimiento/frm_tipo_movimiento.html'

class update_tipo_movimiento(UpdateBase):
  form_class = tipo_movimientoForm
  template_name = 'tipo_movimiento/frm_tipo_movimiento.html'
  model = tipo_movimiento

@login_required(login_url='login/')
def deletetipo_movimiento(em, id):
  dp = tipo_movimiento.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 