from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import tipo_pago
from ..forms.f_tipo_pago import tipo_pagoForm

class list_tipo_pago(ListBase):
  template_name = 'tipo_pago/list_tipo_pago.html'
  queryset = tipo_pago.objects.filter(status=True).values()

class add_tipo_pago(SaveBase):
  form_class = tipo_pagoForm
  template_name = 'tipo_pago/frm_tipo_pago.html'

class update_tipo_pago(UpdateBase):
  form_class = tipo_pagoForm
  template_name = 'tipo_pago/frm_tipo_pago.html'
  model = tipo_pago

@login_required(login_url='login/')
def deletetipo_pago(em, id):
  dp = tipo_pago.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 