from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import conceptos
from ..forms.f_conceptos import conceptosForm

class list_conceptos(ListBase):
  template_name = 'conceptos/list_conceptos.html'
  queryset = conceptos.objects.filter(status=True).values()

class add_conceptos(SaveBase):
  form_class = conceptosForm
  template_name = 'conceptos/frm_conceptos.html'

class update_conceptos(UpdateBase):
  form_class = conceptosForm
  template_name = 'conceptos/frm_conceptos.html'
  model = conceptos

@login_required(login_url='login/')
def deleteconceptos(em, id):
  dp = conceptos.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 