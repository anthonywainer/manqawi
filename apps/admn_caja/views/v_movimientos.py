from django.contrib.auth.decorators import login_required
from django.shortcuts import render,HttpResponse
from ..models import cajas,sesion_caja,movimiento,comprobante,conceptos

from django.db.models import Max,Count
from datetime import datetime

today  = datetime.now()
date   = today.strftime("%Y-%m-%d")

@login_required(login_url='login/')
def movimientos_hoy(request):
  listamovimientos = movimiento.objects.filter(fecha=date,status=True).order_by('-id')
  data = {'listamovimientos': listamovimientos}
  return render(request, 'movimientos/movimientos_hoy.html', data)

@login_required(login_url='login/')
def movimientos_historial(request):
  listamovimientos = movimiento.objects.filter(status=True).order_by('-id')
  data = {'listamovimientos': listamovimientos}
  return render(request, 'movimientos/movimientos_historial.html', data)

@login_required(login_url='login/')
def add_movimiento(request):
    comprobanteslista = comprobante.objects.filter(status=True)
    data = {'comprobanteslista':comprobanteslista}
    return render(request, 'movimientos/nuevo.html', data)

@login_required(login_url='login/')
def extornarmovi(request):
    if request.is_ajax():
        cajaaperturada = sesion_caja.objects.filter(idusuario_id=request.user.id, status=True)
        for obj in cajaaperturada:
            cajainfo = cajas.objects.filter(id=int(obj.idcaja_id))
            for caj in cajainfo:
                saldoingresos = caj.montoingresos
                saldoegresos = caj.montoegresos
                saldototal = caj.montototal

            if (int(request.GET['tipomovimiento']) == 1):
                saldoingresos = float(saldoingresos) - float(request.GET['monto'])
                saldoegresos = float(saldoegresos)
                saldototal = float(saldototal) - float(request.GET['monto'])
            else:
                saldoingresos = float(saldoingresos)
                saldoegresos = float(saldoegresos) - float(request.GET['monto'])
                saldototal = float(saldototal) + float(request.GET['monto'])

            upcajas = cajas.objects.get(id=int(obj.idcaja_id))
            upcajas.montoingresos = saldoingresos
            upcajas.montoegresos = saldoegresos
            upcajas.montototal = saldototal
            upcajas.save()

            movi = movimiento.objects.get(id=int(request.GET['id']))
            movi.status = False
            movi.save()

            return HttpResponse('true')

def validarcaja(request):
    if request.is_ajax():
        cajaaperturada = sesion_caja.objects.filter(idusuario_id=request.user.id,status=True)
        info = "aperturar"
        for obj in cajaaperturada:
            cajainfo = cajas.objects.filter(id=obj.idcaja_id)
            for ob in cajainfo:
                if str(obj.fecha) == str(date):
                    info = "aperturada"
                else:
                    info = "aperturarfecha"

        return HttpResponse(info)

def conceptos_tipo(request):
    if request.is_ajax():
        lisconceptos = conceptos.objects.filter(idtipo_movimiento_id=request.GET['id'])
        html = "<option value=''>Seleccione</option>"
        for obj in lisconceptos:
            html += "<option value='" + str(obj.id) + "'>" + obj.descripcion + "</option>"

        return HttpResponse(html)


def guardarmovimiento(request):
    if request.is_ajax():
        cajaaperturada = sesion_caja.objects.filter(idusuario_id=request.user.id,status=True)
        for obj in cajaaperturada:
            cajainfo = cajas.objects.filter(id=int(obj.idcaja_id))
            for caj in cajainfo:
                saldoingresos = caj.montoingresos
                saldoegresos = caj.montoegresos
                saldototal = caj.montototal

            if (int(request.GET['tipomovimiento']) == 1):
                saldoingresos = float(saldoingresos) + float(request.GET['monto'])
                saldoegresos = float(saldoegresos)
                saldototal = float(saldototal) + float(request.GET['monto'])
            else:
                saldoingresos = float(saldoingresos)
                saldoegresos = float(saldoegresos) + float(request.GET['monto'])
                saldototal = float(saldototal) - float(request.GET['monto'])

            upcajas = cajas.objects.get(id=int(obj.idcaja_id))
            upcajas.montoingresos = saldoingresos
            upcajas.montoegresos = saldoegresos
            upcajas.montototal = saldototal
            upcajas.save()

            n_movi = movimiento()
            n_movi.created_at = date
            n_movi.updated_at = date
            n_movi.descripcion = request.GET['descripcion']
            n_movi.fecha = date
            n_movi.monto = request.GET['monto']
            n_movi.nrocomprobante = request.GET['nrocomprobante']
            n_movi.idcomprobante_id = int(request.GET['comprobante'])
            n_movi.idconcepto_id = int(request.GET['concepto'])
            n_movi.idsesioncaja_id = obj.id
            n_movi.save()

        return HttpResponse("true")