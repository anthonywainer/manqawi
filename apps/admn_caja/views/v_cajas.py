from django.contrib.auth.decorators import login_required
from django.shortcuts import render,HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import cajas,sesion_caja
from ..forms.f_cajas import cajasForm

from django.db.models import Max,Count
from datetime import datetime

today  = datetime.now()
date   = today.strftime("%Y-%m-%d")

class list_cajas(ListBase):
  template_name = 'cajas/list_cajas.html'
  queryset = cajas.objects.filter(status=True).values()

class add_cajas(SaveBase):
  form_class = cajasForm
  template_name = 'cajas/frm_cajas.html'

class update_cajas(UpdateBase):
  form_class = cajasForm
  template_name = 'cajas/frm_cajas.html'
  model = cajas

@login_required(login_url='login/')
def deletecajas(em, id):
  dp = cajas.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS')

@login_required(login_url='login/')
def listasesiones(request):
  listacajas = cajas.objects.filter(status=True)
  lista = sesion_caja.objects.filter(idusuario_id=request.user.id).order_by("-id")
  cajaestado = sesion_caja.objects.filter(idusuario_id=request.user.id,status=True)
  caja_estado = 'false'
  for obj in cajaestado:
    caja_estado = 'true'

  data = {'listasesiones': lista,'listacajas':listacajas, 'caja_estado': caja_estado}
  return render(request, 'cajas/sesiones.html', data)


def aperturar(request):
  if request.is_ajax():
    montoapertura = 0
    sesionid = sesion_caja.objects.all().aggregate(Max('id'))
    sesionid = sesionid['id__max']
    sesiones = sesion_caja.objects.filter(id=sesionid)
    for obj in sesiones:
      montoapertura = obj.montocierre

    apertura = sesion_caja()
    apertura.created_at = date
    apertura.updated_at = date
    apertura.fecha = date
    apertura.status = True
    apertura.montoapertura = montoapertura
    apertura.montocierre = 0.00
    apertura.idcaja_id = request.GET["caja"]
    apertura.idusuario_id = request.user.id
    apertura.save()
    return HttpResponse('true')


def cerrar(request):
  if request.is_ajax():
    cajaestado = sesion_caja.objects.filter(idusuario_id=request.user.id,status=True)
    for obj in cajaestado:
      idsesion = obj.id
      idcajaactual = obj.idcaja_id

    cajainfo = cajas.objects.filter(id=int(idcajaactual))
    for caj in cajainfo:
      saldocierre = caj.montototal

    cierre = sesion_caja.objects.get(id=int(idsesion))
    cierre.montocierre = saldocierre
    cierre.status = False
    cierre.save()
    return HttpResponse('true')