from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import comprobante
from ..forms.f_comprobante import comprobanteForm

class list_comprobante(ListBase):
  template_name = 'comprobante/list_comprobante.html'
  queryset = comprobante.objects.filter(status=True).values()

class add_comprobante(SaveBase):
  form_class = comprobanteForm
  template_name = 'comprobante/frm_comprobante.html'

class update_comprobante(UpdateBase):
  form_class = comprobanteForm
  template_name = 'comprobante/frm_comprobante.html'
  model = comprobante

@login_required(login_url='login/')
def deletecomprobante(em, id):
  dp = comprobante.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 