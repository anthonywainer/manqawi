from django import forms
from ..models import comprobante

class comprobanteForm(forms.ModelForm):
  class Meta():
    model = comprobante
    fields = '__all__'
    exclude = ('status', 'deleted_at')