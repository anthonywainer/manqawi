from django import forms
from ..models import conceptos

class conceptosForm(forms.ModelForm):
  class Meta():
    model = conceptos
    fields = '__all__'
    exclude = ('status', 'deleted_at')