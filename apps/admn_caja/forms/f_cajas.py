from django import forms
from ..models import cajas

class cajasForm(forms.ModelForm):
  class Meta():
    model = cajas
    fields = '__all__'
    exclude = ('status', 'deleted_at')