from django import forms
from ..models import tipo_pago

class tipo_pagoForm(forms.ModelForm):
  class Meta():
    model = tipo_pago
    fields = '__all__'
    exclude = ('status', 'deleted_at')