# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-10 02:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('servicio', '0006_merge_20170109_0026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productos',
            name='precio',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
    ]
