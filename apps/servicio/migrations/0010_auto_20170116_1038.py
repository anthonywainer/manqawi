# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-16 15:38
from __future__ import unicode_literals

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('admn_caja', '0005_auto_20170104_1531'),
        ('servicio', '0009_auto_20170113_2124'),
    ]

    operations = [
        migrations.CreateModel(
            name='cuotaspago',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('nrocuota', models.IntegerField()),
                ('montocuota', models.DecimalField(decimal_places=2, max_digits=10)),
                ('montocancelado', models.DecimalField(decimal_places=2, max_digits=10)),
                ('fechavencimiento', models.DateField()),
                ('fechacancelado', models.DateField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='detalle_amortizacionpago',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('monto', models.DecimalField(decimal_places=2, max_digits=10)),
                ('idcuota', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicio.cuotaspago')),
                ('idmovimiento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admn_caja.movimiento')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='detalle_pedidopago',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('monto', models.DecimalField(decimal_places=2, max_digits=10)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='pagos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('fecha', models.DateTimeField(default=datetime.datetime.now)),
                ('monto', models.DecimalField(decimal_places=2, max_digits=10)),
                ('montoigv', models.DecimalField(decimal_places=2, max_digits=10)),
                ('cliente_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='detalle_pedidopago',
            name='idpago',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicio.pagos'),
        ),
        migrations.AddField(
            model_name='detalle_pedidopago',
            name='idpedido',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicio.pedidos'),
        ),
        migrations.AddField(
            model_name='cuotaspago',
            name='idpago',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='servicio.pagos'),
        ),
    ]
