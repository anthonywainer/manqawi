from django.db import models
from apps.core.core import TimeStampedModel
from datetime import datetime

# Create your models here.
class tipo_plato(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def l_tipo_plato(self):
       return tipo_plato.objects.filter(status=True)

class platos(TimeStampedModel):
    descripcion = models.CharField(max_length=150)
    abreviatura = models.CharField(max_length=50)
    precio      = models.DecimalField(decimal_places=2, max_digits=4)
    #estado = models.IntegerField(default=0)  # 0 disponible , 1 no
    tipo_plato  = models.ForeignKey(tipo_plato)

    def __str__(self):
        return self.descripcion

    def l_platos(self):
        return platos.objects.filter(status=True)

class combos(TimeStampedModel):
    descripcion = models.CharField(max_length=150)
    abreviatura = models.CharField(max_length=50)
    descuento = models.DecimalField(decimal_places=2, max_digits=10)
    #estado = models.IntegerField(default=0)  # 0 disponible , 1 no
    precio      = models.DecimalField(decimal_places=2, max_digits=10)

    def __str__(self):
        return self.descripcion

    def l_combos(self):
        return combos.objects.filter(status=True)

class tipo_producto(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def l_tipo_producto(self):
       return tipo_producto.objects.filter(status=True)

class productos(TimeStampedModel):
    descripcion = models.CharField(max_length=150)
    abreviatura = models.CharField(max_length=50)
    precio      = models.DecimalField(decimal_places=2, max_digits=10)
    #estado = models.IntegerField(default=0)  # 0 disponible , 1 no
    tipo_producto = models.ForeignKey(tipo_producto)

    def __str__(self):
        return self.descripcion

    def l_productos(self):
        return productos.objects.filter(status=True)

class tipo_mesa(TimeStampedModel):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def l_tipo_mesa(self):
       return tipo_mesa.objects.filter(status=True)

class mesas(TimeStampedModel):
    descripcion = models.CharField(max_length=150)
    capacidad = models.IntegerField()
    estado = models.IntegerField(default=0)#0 libre , 1 ocupado , 2 unido
    color = models.IntegerField()
    tipo_mesa = models.ForeignKey(tipo_mesa)

    def __str__(self):
        return self.descripcion

    def l_mesas(self):
        return mesas.objects.filter(status=True)

class pedidos(TimeStampedModel):
    correlativo = models.CharField(max_length=150)
    fecha = models.DateTimeField(default=datetime.now)
    estado = models.IntegerField() #0 iniciado , 1 registrado , 2 atendido , 3 pagado
    sub_total= models.DecimalField(decimal_places=2, max_digits=10)
    registrador = models.ForeignKey('security.Users',related_name='pk_registrador')
    mozo = models.ForeignKey('security.Users',related_name='pk_mozo')
    mesas = models.ForeignKey(mesas)

    def __str__(self):
        return self.correlativo

    def l_pedidos(self):
        return pedidos.objects.filter(status=True)

class info_pedido(TimeStampedModel):
    descripcion = models.CharField(max_length=150) # telfono,direccion, etc
    value = models.CharField(max_length=150) # 973949944,Jr. Mariscal Sucre, etc
    pedidos = models.ForeignKey(pedidos)

    def __str__(self):
        return self.descripcion

    def l_info_ped(self):
        return mesas.objects.filter(status=True)

class det_com_pla(models.Model):
    combos = models.ForeignKey(combos)
    platos = models.ForeignKey(platos)
    cantidad = models.IntegerField()

class det_com_pro(models.Model):
    combos = models.ForeignKey(combos)
    productos = models.ForeignKey(productos)
    cantidad = models.IntegerField()

class det_ped_pla(models.Model):
    pedidos = models.ForeignKey(pedidos)
    platos = models.ForeignKey(platos)
    cantidad = models.IntegerField()
    precio = models.DecimalField(decimal_places=2, max_digits=10)
    fecha_reg = models.DateTimeField()
    fecha_ent = models.DateTimeField()

class det_ped_pro(models.Model):
    pedidos = models.ForeignKey(pedidos)
    productos = models.ForeignKey(productos)
    cantidad = models.IntegerField()
    precio = models.DecimalField(decimal_places=2, max_digits=10)
    fecha_reg = models.DateTimeField()
    fecha_ent = models.DateTimeField()

class det_ped_com(models.Model):
    pedidos = models.ForeignKey(pedidos)
    combos = models.ForeignKey(combos)
    cantidad = models.IntegerField()
    precio = models.DecimalField(decimal_places=2, max_digits=10)
    fecha_reg = models.DateTimeField()
    fecha_ent = models.DateTimeField()

#Tablas creadas por mi pes #
class pagos(TimeStampedModel):
    idcliente = models.ForeignKey('security.Users')
    fecha = models.DateTimeField(default=datetime.now)
    monto = models.DecimalField(decimal_places=2, max_digits=10)
    montoigv = models.DecimalField(decimal_places=2, max_digits=10)

    def __str__(self):
        return self.monto

    def l_pagos(self):
        return pagos.objects.filter(status=True)

class detalle_pedidopago(TimeStampedModel):
    total = models.DecimalField(decimal_places=2, max_digits=10)
    montoigv = models.DecimalField(decimal_places=2, max_digits=10,blank=True,null=True)
    idpedido=models.ForeignKey(pedidos)
    idpago=models.ForeignKey(pagos)

class cuotaspago(TimeStampedModel):
    nrocuota=models.IntegerField()
    montocuota=models.DecimalField(decimal_places=2, max_digits=10)
    montocancelado=models.DecimalField(decimal_places=2, max_digits=10)
    fechavencimiento=models.DateTimeField(default=datetime.now)
    fechacancelado=models.DateTimeField(default=datetime.now)
    idpago=models.ForeignKey(pagos)

class detalle_amortizacionpago(TimeStampedModel):
    monto=models.DecimalField(decimal_places=2, max_digits=10)
    idmovimiento=models.ForeignKey('admn_caja.movimiento')
    idcuota=models.ForeignKey(cuotaspago)