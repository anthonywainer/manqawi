from django import forms
from ..models import mesas

class mesasForm(forms.ModelForm):
  class Meta():
    model = mesas
    fields = '__all__'
    exclude = ('status', 'deleted_at','estado')