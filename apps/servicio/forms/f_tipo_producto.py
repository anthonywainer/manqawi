from django import forms
from ..models import tipo_producto

class tipo_productoForm(forms.ModelForm):
  class Meta():
    model = tipo_producto
    fields = '__all__'
    exclude = ('status', 'deleted_at')