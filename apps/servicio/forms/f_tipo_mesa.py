from django import forms
from ..models import tipo_mesa

class tipo_mesaForm(forms.ModelForm):
  class Meta():
    model = tipo_mesa
    fields = '__all__'
    exclude = ('status', 'deleted_at')