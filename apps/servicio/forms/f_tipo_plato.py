from django import forms
from ..models import tipo_plato

class tipo_platoForm(forms.ModelForm):
  class Meta():
    model = tipo_plato
    fields = '__all__'
    exclude = ('status', 'deleted_at')