from django import forms
from ..models import combos

class combosForm(forms.ModelForm):
  class Meta():
    model = combos
    fields = '__all__'
    exclude = ('status', 'deleted_at')