from django import forms
from ..models import platos

class platosForm(forms.ModelForm):
  class Meta():
    model = platos
    fields = '__all__'
    exclude = ('status', 'deleted_at')