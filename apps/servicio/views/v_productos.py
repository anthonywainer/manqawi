from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import productos
from ..forms.f_productos import productosForm

class list_productos(ListBase):
  template_name = 'productos/list_productos.html'
  queryset = productos.objects.filter(status=True).values()

class add_productos(SaveBase):
  form_class = productosForm
  template_name = 'productos/frm_productos.html'

class update_productos(UpdateBase):
  form_class = productosForm
  template_name = 'productos/frm_productos.html'
  model = productos

@login_required(login_url='login/')
def deleteproductos(em, id):
  dp = productos.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 