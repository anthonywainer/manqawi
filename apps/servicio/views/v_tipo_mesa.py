from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import tipo_mesa
from ..forms.f_tipo_mesa import tipo_mesaForm

class list_tipo_mesa(ListBase):
  template_name = 'tipo_mesa/list_tipo_mesa.html'
  queryset = tipo_mesa.objects.filter(status=True).values()

class add_tipo_mesa(SaveBase):
  form_class = tipo_mesaForm
  template_name = 'tipo_mesa/frm_tipo_mesa.html'

class update_tipo_mesa(UpdateBase):
  form_class = tipo_mesaForm
  template_name = 'tipo_mesa/frm_tipo_mesa.html'
  model = tipo_mesa

@login_required(login_url='login/')
def deletetipo_mesa(em, id):
  dp = tipo_mesa.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 