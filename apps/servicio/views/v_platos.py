from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import platos
from ..forms.f_platos import platosForm

class list_platos(ListBase):
  template_name = 'platos/list_platos.html'
  queryset = platos.objects.filter(status=True).values()

class add_platos(SaveBase):
  form_class = platosForm
  template_name = 'platos/frm_platos.html'

class update_platos(UpdateBase):
  form_class = platosForm
  template_name = 'platos/frm_platos.html'
  model = platos

@login_required(login_url='login/')
def deleteplatos(em, id):
  dp = platos.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 