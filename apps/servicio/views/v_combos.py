from django.contrib.auth.decorators import login_required
from django.shortcuts import render,HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import combos,tipo_plato,platos,tipo_producto,productos,det_com_pla,det_com_pro
from ..forms.f_combos import combosForm
from datetime import datetime
import json

today  = datetime.now()
date   = today.strftime("%Y-%m-%d")

class list_combos(ListBase):
  template_name = 'combos/list_combos.html'
  queryset = combos.objects.filter(status=True).values()

class add_combos(SaveBase):
  form_class = combosForm
  template_name = 'combos/frm_combos.html'

  def get_context_data(self, **kwargs):
    context = super(SaveBase, self).get_context_data(**kwargs)
    context['name'] = 'Registrar'
    listatiposplatos = tipo_plato.objects.filter(status=True).order_by('id')
    listatipoproductos = tipo_producto.objects.filter(status=True).order_by('id')
    platitos = []
    productitos = []
    for obj in listatiposplatos:
      lista = platos.objects.filter(tipo_plato_id=obj.id, status=True)
      platitos.append({'tipo':obj.descripcion,'idtipo':obj.id,'lista':lista})

    for obj in listatipoproductos:
      lista = productos.objects.filter(tipo_producto_id=obj.id, status=True)
      productitos.append({'tipo':obj.descripcion,'idtipo':obj.id,'lista':lista})

    context['listaplatitos'] = platitos
    context['listaproductitos'] = productitos
    return context

class update_combos(UpdateBase):
  form_class = combosForm
  template_name = 'combos/frm_combos.html'
  model = combos

  def get_context_data(self, **kwargs):
    context = super(UpdateBase, self).get_context_data(**kwargs)
    context['name'] = 'Actualizar'
    listatiposplatos = tipo_plato.objects.filter(status=True).order_by('id')
    listatipoproductos = tipo_producto.objects.filter(status=True).order_by('id')
    platitos = []
    productitos = []
    for obj in listatiposplatos:
      lista = platos.objects.filter(tipo_plato_id=obj.id, status=True)
      platitos.append({'tipo': obj.descripcion, 'idtipo': obj.id, 'lista': lista})

    for obj in listatipoproductos:
      lista = productos.objects.filter(tipo_producto_id=obj.id, status=True)
      productitos.append({'tipo': obj.descripcion, 'idtipo': obj.id, 'lista': lista})

    context['listaplatitos'] = platitos
    context['listaproductitos'] = productitos
    return context

@login_required(login_url='login/')
def deletecombos(em, id):
  dp = combos.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS')

@login_required(login_url='login/')
def guardarcombo(request):
  if request.is_ajax():
    if request.GET["id"]=="":
      n_combo = combos()
    else:
      n_combo = combos.objects.get(pk=request.GET["id"])

    n_combo.created_at = date
    n_combo.updated_at = date
    n_combo.descripcion = request.GET['descripcion']
    n_combo.abreviatura = request.GET['abreviatura']
    n_combo.descuento = request.GET['descuento']
    n_combo.precio = request.GET['precio']
    n_combo.save()

    if request.GET["id"]=="":
      idcombovar = combos.objects.latest('id')
      idcombo = idcombovar.id
    else:
      idcombo = request.GET["id"]
      eli_detpla = det_com_pla.objects.filter(combos_id=int(idcombo))
      eli_detpla.delete()
      eli_detpro = det_com_pro.objects.filter(combos_id=int(idcombo))
      eli_detpro.delete()

    cantidadpl = request.GET.getlist('cantidadplato[]')
    cantidadpr = request.GET.getlist('cantidadproducto[]')
    cant = 0

    for i in request.GET.getlist('idplato[]'):
      n_deta = det_com_pla()
      n_deta.cantidad = cantidadpl[cant]
      n_deta.combos_id = idcombo
      n_deta.platos_id = i
      n_deta.save()
      cant = cant + 1

    cant = 0
    for i in request.GET.getlist('idproducto[]'):
      n_deta = det_com_pro()
      n_deta.cantidad = cantidadpr[cant]
      n_deta.combos_id = idcombo
      n_deta.productos_id = i
      n_deta.save()
      cant = cant + 1

    return HttpResponse('true')

def infocombo(request):
  listapla=[]
  listapro=[]
  listapla = det_com_pla.objects.filter(combos_id=int(request.GET["id"])).values\
    ('platos__descripcion','cantidad','platos_id')
  listapro = det_com_pro.objects.filter(combos_id=int(request.GET["id"])).values\
    ('productos__descripcion','cantidad','productos_id')
  data=[
    {
      'platos':list(listapla),'productos':list(listapro)
    }
  ]
  return HttpResponse(json.dumps(data))