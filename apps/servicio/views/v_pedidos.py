from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse,render
from apps.core.crud import ListBase, SaveBase, UpdateBase,  TemplateView
from ..models import *
from ...admn_caja.models import cajas,sesion_caja,movimiento
from ..forms.f_pedidos import pedidosForm
from django.core.serializers.json import DjangoJSONEncoder
from datetime import datetime
from decimal import Decimal
from django.db.models import F,Value,Max

today  = datetime.now()
date   = today.strftime("%Y-%m-%d")

class list_pedidos(ListBase):
  template_name = 'pedidos/list_pedidos.html'
  queryset = pedidos.objects.filter(status=True).values()

class add_pedidos(SaveBase):
  form_class = pedidosForm
  template_name = 'pedidos/frm_pedidos.html'

class update_pedidos(UpdateBase):
  form_class = pedidosForm
  template_name = 'pedidos/frm_pedidos.html'
  model = pedidos

@login_required(login_url='login/')
def deletepedidos(em, id):
  dp = pedidos.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS')

class do_pedidos(SaveBase):
  template_name = 'pedidos/do_pedidos.html'
  form_class = pedidosForm

  def get_context_data(self, **kwargs):
    context = super(SaveBase, self).get_context_data(**kwargs)
    context['name'] = 'Registrar'
    context['iddefault'] = 1
    return context

  def post(self, request, *args, **kwargs):
      try:
          idpedido = pedidos.objects.latest('id')
          ultid=idpedido.id
      except pedidos.DoesNotExist:
          ultid = 1

      n_pedido = pedidos()
      n_pedido.correlativo = 'Pedido_'+str( int(ultid)+1)
      preciopl = request.POST.getlist('precioplatos[]')
      preciopr = request.POST.getlist('precioproductos[]')
      preciocom = request.POST.getlist('preciocombos[]')
      cantidadpl = request.POST.getlist('cantidadplatos[]')
      cantidadpr = request.POST.getlist('cantidadproductos[]')
      cantidadcom = request.POST.getlist('cantidadcombos[]')
      for i in range(0, len(preciopl)):
          preciopl[i] =float(preciopl[i])* int(cantidadpl[i])

      for i in range(0, len(preciopr)):
          preciopr[i] =float(preciopr[i]) * int(cantidadpr[i])

      for i in range(0, len(preciocom)):
          preciocom[i] =float(preciocom[i]) * int(cantidadcom[i])

      sub_t =sum([float(i) for i in preciopl])+sum([float(i) for i in preciopr])+sum([float(i) for i in preciocom])
      n_pedido.sub_total = Decimal(sub_t)
      n_pedido.estado = 0 #0 iniciado , 1 registrado , 2 atendido , 3 pagado
      n_pedido.registrador_id = request.user.id
      n_pedido.mozo_id = 1
      n_pedido.mesas_id = int(request.POST['mesas'])
      n_pedido.save()
      idpedido = pedidos.objects.latest('id')
      mm=mesas.objects.get(id=int(request.POST['mesas']))
      mm.estado=1
      mm.save()
      cant = 0
      for i in request.POST.getlist('idplatos[]'):
          n_deta = det_ped_pla()
          n_deta.cantidad = cantidadpl[cant]
          n_deta.pedidos_id = idpedido.id
          n_deta.platos_id = i
          n_deta.precio = preciopl[cant]
          n_deta.fecha_reg = date
          n_deta.fecha_ent = date
          n_deta.save()
          cant = cant + 1

      cant = 0
      for i in request.POST.getlist('idproductos[]'):
          n_deta = det_ped_pro()
          n_deta.cantidad = cantidadpr[cant]
          n_deta.pedidos_id = idpedido.id
          n_deta.productos_id = i
          n_deta.precio = preciopr[cant]
          n_deta.fecha_reg = date
          n_deta.fecha_ent = date
          n_deta.save()
          cant = cant + 1

      cant = 0
      for i in request.POST.getlist('idcombos[]'):
          n_deta = det_ped_com()
          n_deta.cantidad = cantidadcom[cant]
          n_deta.pedidos_id = idpedido.id
          n_deta.combos_id = i
          n_deta.precio = preciocom[cant]
          n_deta.fecha_reg = date
          n_deta.fecha_ent = date
          n_deta.save()
          cant = cant + 1

      return HttpResponse('true')

import json
@login_required(login_url='login/')
def json_cartas(r):
    listatiposplatos = tipo_plato.objects.filter(status=True).order_by('id')
    listatipoproductos = tipo_producto.objects.filter(status=True).order_by('id')
    platitos = []
    productitos = []
    carta = []

    for obj in listatiposplatos:
      lista = platos.objects.filter(tipo_plato_id=obj.id, status=True).values()
      platitos.append({'clase':'platos','tipo': obj.descripcion, 'idtipo': obj.id,'lista':list(lista)})

    for obj in listatipoproductos:
      lista = productos.objects.filter(tipo_producto_id=obj.id, status=True).values()
      productitos.append({'clase':'productos','tipo': obj.descripcion,'idtipo': obj.id,'lista':list(lista)})

    carta.extend(platitos)
    carta.extend(productitos)
    listac = combos.objects.filter(status=True).order_by('id').values()
    carta.append({'clase':'combos','tipo': 'Combo', 'idtipo': 0, 'lista': list(listac)})

    return HttpResponse(json.dumps(list(carta),cls=DjangoJSONEncoder))

@login_required(login_url='login/')
def json_mesas(val):
    if not val.GET.get('filtro'):
        lmesas = mesas.objects.filter(status=True).order_by('id')
    elif val.GET.get('filtro')== 'estado':
        valor = val.GET.get('valor')
        lmesas = mesas.objects.filter(estado=valor, status=True).order_by('id')
    elif val.GET.get('filtro') == 'id':
        valor = val.GET.get('valor')
        lmesas = mesas.objects.filter(id=valor,estado=1, status=True).order_by('id')
    datos = []
    for obj in lmesas:

        if obj.estado==1:
            id_ped = mesas.objects.filter(id=obj.id, status=True).aggregate(Max('pedidos__id'))
            ped = pedidos.objects.filter(id=int(id_ped['pedidos__id__max']), status=True).values()
            lpla = []
            lcom = []
            lpro = []
            lped = []
            lista = []
            lplatos = det_ped_pla.objects.filter(pedidos_id= id_ped['pedidos__id__max']).annotate(ids=F('platos__id'),desc=F('platos__descripcion')).values('ids','cantidad','desc','precio')
            lcombos = det_ped_com.objects.filter(pedidos_id=id_ped['pedidos__id__max']).annotate(ids=F('combos__id'),desc=F('combos__descripcion')).values('ids','cantidad','desc','precio')
            lproductos = det_ped_pro.objects.filter(pedidos_id=id_ped['pedidos__id__max']).annotate(ids=F('productos__id'),desc=F('productos__descripcion')).values('ids','cantidad','desc','precio')

            if lplatos.exists():
                lpla.insert(0,(lplatos[0],{'clase': 'platos'}))

            if lcombos.exists():
                lcom.insert(0, (lcombos[0], {'clase': 'combos'}))

            if lproductos.exists():
                lpro.insert(0, (lproductos[0], {'clase': 'productos'}))
            lped.extend(list(ped))
            lista.extend(list(lpla))
            lista.extend(list(lcom))
            lista.extend(list(lpro))

            datos.append({'id': obj.id,'descripcion': obj.descripcion,'estado': obj.estado,'lista': list(lista),'pedido':list(lped)})
        else:
            datos.append({'id': obj.id,'descripcion': obj.descripcion,'estado': obj.estado })

    print(datos)
    return HttpResponse(json.dumps(list(datos),cls=DjangoJSONEncoder))

@login_required(login_url='login/')
def info_mesapagar(request):
    datos = []
    lista = []
    valor = request.GET.get('mesa')
    pedido_id = pedidos.objects.filter(mesas_id=int(valor)).aggregate(Max('id'))
    pedido_id = pedido_id['id__max']

    pedidoinfo = pedidos.objects.filter(id=int(pedido_id)).values()

    lplatos = det_ped_pla.objects.filter(pedidos_id=pedido_id).annotate(
        desc=F('platos__descripcion')).values('id','cantidad','precio','desc')
    lcombos = det_ped_com.objects.filter(pedidos_id=pedido_id).annotate(
        desc=F('combos__descripcion')).values('id','cantidad','precio','desc')
    lproductos = det_ped_pro.objects.filter(pedidos_id=pedido_id).annotate(
        desc=F('productos__descripcion')).values('id','cantidad','precio','desc')

    lista.extend(list(lplatos))
    lista.extend(list(lcombos))
    lista.extend(list(lproductos))

    mesita = mesas.objects.filter(id=int(valor))
    for obj in mesita:
        datos.append({'id': obj.id, 'descripcion': obj.descripcion,'lista': list(lista),'pedidoinfo':list(pedidoinfo)})

    return HttpResponse(json.dumps(list(datos), cls=DjangoJSONEncoder))

def realizarpago(request):
    igv = float(request.POST['subtotal'])*0.18
    n_pagos = pagos()
    n_pagos.idcliente_id = int(3)
    n_pagos.monto = float(request.POST['total'])
    n_pagos.montoigv = float(igv)
    n_pagos.save()
    pagito = pagos.objects.latest('id')
    idpago = pagito.id

    n_deta = detalle_pedidopago()
    n_deta.total = float(request.POST['subtotal'])
    n_deta.montoigv = float(igv)
    n_deta.idpago_id = int(idpago)
    n_deta.idpedido_id = int(request.POST['pedido_id'])
    n_deta.save()

    n_cuo = cuotaspago()
    n_cuo.nrocuota = int(1)
    n_cuo.montocuota = float(request.POST['total'])
    n_cuo.montocancelado = float(request.POST['total'])
    n_cuo.idpago_id = int(idpago)
    n_cuo.save()
    cuotita = cuotaspago.objects.latest('id')
    idcuotita = cuotita.id

    cajaaperturada = sesion_caja.objects.filter(idusuario_id=request.user.id, status=True)
    for obj in cajaaperturada:
        cajainfo = cajas.objects.filter(id=int(obj.idcaja_id))
        for caj in cajainfo:
            saldoingresos = caj.montoingresos
            saldoegresos = caj.montoegresos
            saldototal = caj.montototal

        saldoingresos = float(saldoingresos) + float(request.POST['total'])
        saldoegresos = float(saldoegresos)
        saldototal = float(saldototal) + float(request.POST['total'])

        upcajas = cajas.objects.get(id=int(obj.idcaja_id))
        upcajas.montoingresos = saldoingresos
        upcajas.montoegresos = saldoegresos
        upcajas.montototal = saldototal
        upcajas.save()

        n_movi = movimiento()
        n_movi.descripcion = 'INGRESO POR PAGO DE PEDIDO'
        n_movi.fecha = date
        n_movi.monto = request.POST['total']
        n_movi.nrocomprobante = request.POST['num_comprobante']
        n_movi.idcomprobante_id = int(request.POST['comprobante'])
        n_movi.idconcepto_id = int(1)
        n_movi.idsesioncaja_id = obj.id
        n_movi.save()
        movimientito = movimiento.objects.latest('id')
        idmovimientito = movimientito.id

    n_amortiza = detalle_amortizacionpago()
    n_amortiza.idcuota_id = int(idcuotita)
    n_amortiza.idmovimiento_id = int(idmovimientito)
    n_amortiza.monto = float(request.POST['total'])
    n_amortiza.save()

    dp = mesas.objects.get(pk=request.POST['mesita_id'])
    dp.estado = int(0)
    dp.save()

    dp = pedidos.objects.get(pk=request.POST['pedido_id'])
    dp.estado = int(3)
    dp.save()
    return HttpResponse(idpago)

def imprimirticket(request):
    info = cuotaspago.objects.filter(idpago_id=int(request.GET['id']))
    for ob in info:
        vermovi = detalle_amortizacionpago.objects.filter(idcuota_id=int(ob.id))
        for o in vermovi:
            moviid = o.idmovimiento_id

    movi = movimiento.objects.filter(id=int(moviid))
    data = {'movi': movi}
    return render(request, 'pedidos/ticket.html', data)
