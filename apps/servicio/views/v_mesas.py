from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import mesas
from ..forms.f_mesas import mesasForm

class list_mesas(ListBase):
  template_name = 'mesas/list_mesas.html'
  queryset = mesas.objects.filter(status=True).values()

class add_mesas(SaveBase):
  form_class = mesasForm
  template_name = 'mesas/frm_mesas.html'

class update_mesas(UpdateBase):
  form_class = mesasForm
  template_name = 'mesas/frm_mesas.html'
  model = mesas

@login_required(login_url='login/')
def deletemesas(em, id):
  dp = mesas.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 