from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import tipo_plato
from ..forms.f_tipo_plato import tipo_platoForm

class list_tipo_plato(ListBase):
  template_name = 'tipo_plato/list_tipo_plato.html'
  queryset = tipo_plato.objects.filter(status=True).values()

class add_tipo_plato(SaveBase):
  form_class = tipo_platoForm
  template_name = 'tipo_plato/frm_tipo_plato.html'

class update_tipo_plato(UpdateBase):
  form_class = tipo_platoForm
  template_name = 'tipo_plato/frm_tipo_plato.html'
  model = tipo_plato

@login_required(login_url='login/')
def deletetipo_plato(em, id):
  dp = tipo_plato.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 