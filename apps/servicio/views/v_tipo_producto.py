from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse
from apps.core.crud import ListBase, SaveBase, UpdateBase
from ..models import tipo_producto
from ..forms.f_tipo_producto import tipo_productoForm

class list_tipo_producto(ListBase):
  template_name = 'tipo_producto/list_tipo_producto.html'
  queryset = tipo_producto.objects.filter(status=True).values()

class add_tipo_producto(SaveBase):
  form_class = tipo_productoForm
  template_name = 'tipo_producto/frm_tipo_producto.html'

class update_tipo_producto(UpdateBase):
  form_class = tipo_productoForm
  template_name = 'tipo_producto/frm_tipo_producto.html'
  model = tipo_producto

@login_required(login_url='login/')
def deletetipo_producto(em, id):
  dp = tipo_producto.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 