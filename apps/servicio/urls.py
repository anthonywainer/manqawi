from django.conf.urls import url 
#views.
from .views.v_tipo_plato import list_tipo_plato,add_tipo_plato,update_tipo_plato,deletetipo_plato
from .views.v_platos import list_platos,add_platos,update_platos,deleteplatos
from .views.v_combos import *
from .views.v_tipo_producto import list_tipo_producto,add_tipo_producto,update_tipo_producto,deletetipo_producto
from .views.v_productos import list_productos,add_productos,update_productos,deleteproductos
from .views.v_tipo_mesa import list_tipo_mesa,add_tipo_mesa,update_tipo_mesa,deletetipo_mesa
from .views.v_mesas import list_mesas,add_mesas,update_mesas,deletemesas
from .views.v_pedidos import *
#.

urlpatterns = [  

  # tipo_plato
  url(r'^servicio/tipo_plato/listar/$', list_tipo_plato.as_view()),
  url(r'^servicio/tipo_plato/add/$', add_tipo_plato.as_view()),
  url(r'^servicio/tipo_plato/update/(?P<pk>\d+)/$', update_tipo_plato.as_view()),
  url(r'^servicio/tipo_plato/delete/(?P<id>\d+)/$', deletetipo_plato),


  # platos
  url(r'^servicio/platos/listar/$', list_platos.as_view()),
  url(r'^servicio/platos/add/$', add_platos.as_view()),
  url(r'^servicio/platos/update/(?P<pk>\d+)/$', update_platos.as_view()),
  url(r'^servicio/platos/delete/(?P<id>\d+)/$', deleteplatos),


  # combos
  url(r'^servicio/combos/listar/$', list_combos.as_view()),
  url(r'^servicio/combos/add/$', add_combos.as_view()),
  url(r'^servicio/combos/update/(?P<pk>\d+)/$', update_combos.as_view()),
  url(r'^servicio/combos/delete/(?P<id>\d+)/$', deletecombos),
  url(r'^servicio/combos/guardarcombo/$', guardarcombo),
  url(r'^servicio/combos/infocombo/$', infocombo),

  # tipo_producto
  url(r'^servicio/tipo_producto/listar/$', list_tipo_producto.as_view()),
  url(r'^servicio/tipo_producto/add/$', add_tipo_producto.as_view()),
  url(r'^servicio/tipo_producto/update/(?P<pk>\d+)/$', update_tipo_producto.as_view()),
  url(r'^servicio/tipo_producto/delete/(?P<id>\d+)/$', deletetipo_producto),


  # productos
  url(r'^servicio/productos/listar/$', list_productos.as_view()),
  url(r'^servicio/productos/add/$', add_productos.as_view()),
  url(r'^servicio/productos/update/(?P<pk>\d+)/$', update_productos.as_view()),
  url(r'^servicio/productos/delete/(?P<id>\d+)/$', deleteproductos),


  # tipo_mesa
  url(r'^servicio/tipo_mesa/listar/$', list_tipo_mesa.as_view()),
  url(r'^servicio/tipo_mesa/add/$', add_tipo_mesa.as_view()),
  url(r'^servicio/tipo_mesa/update/(?P<pk>\d+)/$', update_tipo_mesa.as_view()),
  url(r'^servicio/tipo_mesa/delete/(?P<id>\d+)/$', deletetipo_mesa),


  # mesas
  url(r'^servicio/mesas/listar/$', list_mesas.as_view()),
  url(r'^servicio/mesas/add/$', add_mesas.as_view()),
  url(r'^servicio/mesas/update/(?P<pk>\d+)/$', update_mesas.as_view()),
  url(r'^servicio/mesas/delete/(?P<id>\d+)/$', deletemesas),


  # pedidos
  url(r'^servicio/pedidos/listar/$', list_pedidos.as_view()),
  url(r'^servicio/pedidos/add/$', add_pedidos.as_view()),
  url(r'^servicio/pedidos/update/(?P<pk>\d+)/$', update_pedidos.as_view()),
  url(r'^servicio/pedidos/delete/(?P<id>\d+)/$', deletepedidos),
  url(r'^servicio/pedidos/do/$', do_pedidos.as_view()),

  #json
  url(r'^servicio/pedidos/json_cartas/$', json_cartas),
  url(r'^servicio/pedidos/json_mesas/$', json_mesas),

  #rutas para pagos
  url(r'^servicio/pedidos/realizarpago/$', realizarpago),
  url(r'^servicio/pedidos/info_mesapagar/$', info_mesapagar),
  url(r'^servicio/pedidos/imprimirticket/$', imprimirticket),
]